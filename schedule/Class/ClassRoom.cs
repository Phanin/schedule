﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace schedule.Class
{
    class ClassRoom
    {
        public int Id { get; private set; }
        public String className { get; private set; }
        public String departmentName { get; private set; }
        public String grade { get; private set; }
        public String semester { get; private set; }
        public String schoolYear { get; private set; }

        public static void InitializeDbClassRoom()
        {
            MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString);

            Application.ApplicationExit += (sender, args) =>
            {
                if (con != null)
                {
                    con.Dispose();
                    con = null;
                }
            };
        }

        private ClassRoom(int id, String cname, String dname, String smt, String gd, String syear)
        {
            Id = id;
            className = cname; // cname = class name
            departmentName = dname;
            grade = gd;
            semester = smt;
            schoolYear = syear;
        }

        public static List<ClassRoom> GetClassRooms()
        {
            List<ClassRoom> classRooms = new List<ClassRoom>();

            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = "SELECT * FROM class";
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int id = (int)reader["id"];
                    String className = reader["class_name"].ToString();
                    String departmentName = reader["department_name"].ToString();
                    String semester = reader["semester"].ToString();
                    String grade = reader["grade"].ToString();
                    String schoolYear = reader["school_year"].ToString();

                    ClassRoom classRoom = new ClassRoom(id, className, departmentName, semester, grade, schoolYear);
                    
                    classRooms.Add(classRoom);
                }
                reader.Close();

                con.Close();
            }
            return classRooms;
        }

        public static ClassRoom Insert(string cname, string dname, string gd, string smt, string syear)
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = string.Format("INSERT INTO class(class_name, department_name, semester, school_year, grade) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", cname, dname, smt, syear, gd);
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                command.ExecuteNonQuery();

                int id = (int)command.LastInsertedId;

                ClassRoom classRoom = new ClassRoom(id, cname, dname, gd, smt, syear);
                con.Close();

                return classRoom;
            }
        }

        public void Update(string cname, string dname, string smt, string syear)
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = string.Format("UPDATE class SET class_name='{0}', department_name='{1}', semester='{2}', school_year='{3}' WHERE ID='{4}'", cname, dname, smt, syear, Id);
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                command.ExecuteNonQuery();

                con.Close();
            }
        }

        public void Delete()
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = string.Format("DELETE FROM class WHERE ID={0}", Id);
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                command.ExecuteNonQuery();

                con.Close();
            }
        }
    }
}
