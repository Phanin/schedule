﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace schedule.Class
{
    class Subject
    {
        public int Id { get; private set; }
        public String subjectName { get; private set; }
        public String subjectCredits { get; private set; }
        public String isLab { get; private set; }

        public static void InitializeDbSubject()
        {
            MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString);

            Application.ApplicationExit += (sender, args) =>
            {
                if (con != null)
                {
                    con.Dispose();
                    con = null;
                }
            };
        }

        private Subject(int id, String sjname, String sjcd, String islab)
        {
            Id = id;
            subjectName = sjname;
            subjectCredits = sjcd;
            isLab = islab;
        }

        public static List<Subject> GetSubjects()
        {
            List<Subject> subjects = new List<Subject>();

            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = "SELECT * FROM subjects";
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int id = (int)reader["id"];
                    String subjectName = reader["subject_name"].ToString();
                    String subjectCredits = reader["subject_credits"].ToString();
                    String isLab = reader["is_lab"].ToString();
                    if(isLab == "1")
                    {
                        isLab = "Yes";
                    } else
                    {
                        isLab = "No";
                    }

                    Subject subject = new Subject(id, subjectName, subjectCredits, isLab);

                    subjects.Add(subject);
                }
                reader.Close();

                con.Close();
            }
            return subjects;
        }

        public static Subject Insert(string sjname, string sjcd, string islab, string tid)
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = string.Format("INSERT INTO subjects(subject_name, subject_credits, is_lab) VALUES ('{0}', '{1}', '{2}')", sjname, sjcd, islab);
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                command.ExecuteNonQuery();

                int id = (int)command.LastInsertedId;

                String queryTeacherAndSubject = string.Format("INSERT INTO teacher_and_subject(subject_id, teacher_id) VALUES ('{0}', '{1}')", id, tid);
                MySqlCommand commandTeacherAndSubject = new MySqlCommand(queryTeacherAndSubject, con);

                commandTeacherAndSubject.ExecuteNonQuery();

                Subject subject = new Subject(id, sjname, sjcd, islab);
                con.Close();

                return subject;
            }
        }

        public void Update(string sjname, string sjcd, string islab)
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = string.Format("UPDATE subjects SET subject_name='{0}', subject_credits='{1}', is_lab='{2}' WHERE ID='{3}'", sjname, sjcd, islab, Id);
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                command.ExecuteNonQuery();

                con.Close();
            }
        }

        public void Delete()
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = string.Format("DELETE FROM subjects WHERE ID={0}", Id);
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                command.ExecuteNonQuery();

                con.Close();
            }
        }
    }
}
