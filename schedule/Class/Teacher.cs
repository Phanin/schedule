﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace schedule.Class
{
    class Teacher
    {
        public int Id { get; private set; }
        public String Username { get; private set; }
        public String Position { get; private set; }
        public String Email { get; private set; }
        public String Password { get; private set; }
        public String Address { get; private set; }
        public String Phone { get; private set; }
        public String Gender { get; private set; }
        public String Summary { get; private set; }
        public String Status { get; private set; }
        public String userType { get; private set; }
        public static void InitializeDbTeacher()
        {
                MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString);

                Application.ApplicationExit += (sender, args) =>
                {
                    if (con != null)
                    {
                        con.Dispose();
                        con = null;
                    }
                };
        }
        
        private Teacher(int id, String usn, String pst, String eml, String pwd, String stt, String utp, String ph, String sex, String ads, String smr)
        {
            Id = id;
            userType = utp;
            Username = usn;
            Email = eml;
            Password = pwd;
            Position = pst;
            Status = stt;
            Phone = ph;
            Gender = sex;
            Address = ads;
            Summary = smr;
        }

        public static List<Teacher> GetTeachers()
        {
            List<Teacher> teachers = new List<Teacher>();

            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                //String query = "SELECT * FROM users";
                String query = "SELECT * FROM teachers, users WHERE teachers.user_id = users.id ORDER BY teachers.id";
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int id = (int)reader["id"];
                    String usertype = reader["user_type"].ToString();
                    String username = reader["name"].ToString();
                    String position = reader["position"].ToString();
                    String email = reader["email"].ToString();
                    String password = reader["password"].ToString();
                    String status = reader["status"].ToString();
                    String phone = reader["phone_number"].ToString();
                    String gender = reader["gender"].ToString();
                    String address = reader["address"].ToString();
                    String summary = reader["summary"].ToString();

                    Teacher teacher = new Teacher(id, username, position, email, password, status, usertype, phone, gender, address, summary);

                    teachers.Add(teacher);
                }

                reader.Close();

                con.Close();
            }

            return teachers;
        }
        
        public static Teacher Insert(string usn, string eml, string pwd, string pst, string stt, string utp, string ph, string sex, string ads, string smr)
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = string.Format("INSERT INTO users(user_type, name, email, password, status) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", utp, usn, eml, pwd, stt);
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                command.ExecuteNonQuery();
                command.Parameters.Clear();

                int id = (int)command.LastInsertedId;

                String queryTeacher = string.Format("INSERT INTO teachers(user_id, position, phone_number, gender, address, summary) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')",id, pst, ph, sex, ads, smr);
                MySqlCommand commandTeacher = new MySqlCommand(queryTeacher, con);
                commandTeacher.ExecuteNonQuery();

                Teacher teacher = new Teacher(id, utp, usn, eml, pwd, pst, stt, ph, sex, ads, smr);
                con.Close();

                return teacher;
            }
        }
        
        public void Update(string usn, string eml, string pwd, string pst, string stt, string utp, string ph, string sex, string ads, string smr)
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = string.Format("UPDATE users SET name='{0}', email='{1}', password='{2}', status='{3}' WHERE ID='{4}'", usn, eml, pwd, stt, Id);
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                command.ExecuteNonQuery();
                command.Parameters.Clear();

                String queryTeacher = string.Format("UPDATE teachers SET position='{0}', phone_number='{1}', gender='{2}', address='{3}', summary='{4}' WHERE ID='{5}'", pst, ph, sex, ads, smr, Id);
                MySqlCommand commandTeacher = new MySqlCommand(queryTeacher, con);
                commandTeacher.ExecuteNonQuery();

                con.Close();

            }
        }
        
        public void Delete()
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                String query = string.Format("DELETE FROM users WHERE ID={0}", Id);
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                command.ExecuteNonQuery();
                command.Parameters.Clear();

                String queryTeacher = string.Format("DELETE FROM teachers WHERE ID={0}", Id);
                MySqlCommand commandTeacher = new MySqlCommand(queryTeacher, con);
                commandTeacher.ExecuteNonQuery();
                
                con.Close();
            }
        }
    }
}
