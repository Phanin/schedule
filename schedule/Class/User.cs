﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schedule.Class
{
    class User
    {
        public string log_username { set; get; }
        public string log_password { set; get; }
        public Int32 log_role { set; get; }
        public Int32 user_id { set; get; }

        public string GetUserRoleString()
        {
            string role_string = "";

            if (log_role == 0)
            {
                role_string = "super admin";
            }
            else if (log_role == 1)
            {
                role_string = "teacher";
            }
            else
            {
                role_string = "student";
            }

            return role_string;
        }

        public bool User_verification(string username, string password)
        {
            this.log_username = username;
            this.log_password = password;

            bool check = false;

            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {

                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand command = new MySqlCommand("SELECT * FROM `users` WHERE name=@usn AND password=@pass", con);

                command.Parameters.Add("@usn", MySqlDbType.VarChar).Value = log_username;
                command.Parameters.Add("@pass", MySqlDbType.VarChar).Value = log_password;

                MySqlDataReader reader = command.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //log_role = reader.GetInt32(1);
                        log_role = reader.GetInt32(reader.GetOrdinal("user_type"));

                        check = true;
                    }
                }
                con.Close();
            }
            return check;
        }

        public int Pass_userId(string username, string password)
        {
            this.log_username = username;
            this.log_password = password;

            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {

                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand command = new MySqlCommand("SELECT * FROM `users` WHERE name=@usn AND password=@pass", con);

                command.Parameters.Add("@usn", MySqlDbType.VarChar).Value = log_username;
                command.Parameters.Add("@pass", MySqlDbType.VarChar).Value = log_password;

                MySqlDataReader reader = command.ExecuteReader();


                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        user_id = reader.GetInt32(reader.GetOrdinal("id"));
                    }
                }
                con.Close();
            }
            return user_id;
        }

        public DataTable getUserById(Int32 userid)
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlDataAdapter adapter = new MySqlDataAdapter();
                DataTable table = new DataTable();

                MySqlCommand command = new MySqlCommand("SELECT * FROM users, teachers WHERE users.id=@uid AND users.id = teachers.user_id ", con);
                command.Parameters.Add("@uid", MySqlDbType.UInt32).Value = userid;

                adapter.SelectCommand = command;

                adapter.Fill(table);

                con.Close();

                return table;
            }
        }

        public bool updateUser(int userid, string uname, string gender, string address, string email, string pass, string position,string pnumber, string status, string smr)
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                MySqlCommand command = new MySqlCommand("UPDATE users SET name=@uname, email=@email, password=@pass, status=@status WHERE id=@uid", con);
                command.Parameters.Add("@uname", MySqlDbType.VarChar).Value = uname;
                command.Parameters.Add("@email", MySqlDbType.VarChar).Value = email;
                command.Parameters.Add("@pass", MySqlDbType.VarChar).Value = pass;
                command.Parameters.Add("@status", MySqlDbType.VarChar).Value = status;
                command.Parameters.Add("@uid", MySqlDbType.Int32).Value = userid;

                MySqlCommand commandRelate = new MySqlCommand("UPDATE teachers SET position=@position, phone_number=@pnumber, gender=@gender, address=@address, summary=@summary WHERE user_id=@usid", con);
                commandRelate.Parameters.Add("@position", MySqlDbType.VarChar).Value = position;
                commandRelate.Parameters.Add("@pnumber", MySqlDbType.VarChar).Value = pnumber;
                commandRelate.Parameters.Add("@gender", MySqlDbType.VarChar).Value = gender;
                commandRelate.Parameters.Add("@address", MySqlDbType.VarChar).Value = address;
                commandRelate.Parameters.Add("@summary", MySqlDbType.VarChar).Value = smr;
                commandRelate.Parameters.Add("@usid", MySqlDbType.Int32).Value = userid;

                if (commandRelate.ExecuteNonQuery() == 1)
                {
                    con.Close();
                    return true;
                }
                else
                {
                    con.Close();
                    return false;
                }
            }
        }
    }
}
