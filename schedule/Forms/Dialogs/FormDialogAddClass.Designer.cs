﻿
namespace schedule.Forms.Dialogs
{
    partial class FormDialogAddClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addGradeLabel = new System.Windows.Forms.Label();
            this.addSemesterLabel = new System.Windows.Forms.Label();
            this.addSchoolyearLabel = new System.Windows.Forms.Label();
            this.addClassDepartmentLabel = new System.Windows.Forms.Label();
            this.addDepartmentDropBox = new System.Windows.Forms.ComboBox();
            this.addClassRoomEnter = new System.Windows.Forms.TextBox();
            this.addClassRoomNameLabel = new System.Windows.Forms.Label();
            this.labelAddNewClass = new System.Windows.Forms.Label();
            this.exitButton = new FontAwesome.Sharp.IconButton();
            this.createClass = new FontAwesome.Sharp.IconButton();
            this.addGradeDropBox = new System.Windows.Forms.ComboBox();
            this.addSemesterDropBox = new System.Windows.Forms.ComboBox();
            this.dateTimePickerSchoolYears = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // addGradeLabel
            // 
            this.addGradeLabel.AutoSize = true;
            this.addGradeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addGradeLabel.Location = new System.Drawing.Point(403, 137);
            this.addGradeLabel.Name = "addGradeLabel";
            this.addGradeLabel.Size = new System.Drawing.Size(60, 20);
            this.addGradeLabel.TabIndex = 38;
            this.addGradeLabel.Text = "Grade:";
            // 
            // addSemesterLabel
            // 
            this.addSemesterLabel.AutoSize = true;
            this.addSemesterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSemesterLabel.Location = new System.Drawing.Point(572, 137);
            this.addSemesterLabel.Name = "addSemesterLabel";
            this.addSemesterLabel.Size = new System.Drawing.Size(86, 20);
            this.addSemesterLabel.TabIndex = 36;
            this.addSemesterLabel.Text = "Semester:";
            this.addSemesterLabel.Click += new System.EventHandler(this.addTeacherPhoneLabel_Click);
            // 
            // addSchoolyearLabel
            // 
            this.addSchoolyearLabel.AutoSize = true;
            this.addSchoolyearLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSchoolyearLabel.Location = new System.Drawing.Point(26, 137);
            this.addSchoolyearLabel.Name = "addSchoolyearLabel";
            this.addSchoolyearLabel.Size = new System.Drawing.Size(113, 20);
            this.addSchoolyearLabel.TabIndex = 34;
            this.addSchoolyearLabel.Text = "School Years:";
            // 
            // addClassDepartmentLabel
            // 
            this.addClassDepartmentLabel.AutoSize = true;
            this.addClassDepartmentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addClassDepartmentLabel.Location = new System.Drawing.Point(402, 77);
            this.addClassDepartmentLabel.Name = "addClassDepartmentLabel";
            this.addClassDepartmentLabel.Size = new System.Drawing.Size(151, 20);
            this.addClassDepartmentLabel.TabIndex = 33;
            this.addClassDepartmentLabel.Text = "Department Name:";
            this.addClassDepartmentLabel.Click += new System.EventHandler(this.addTeacherGenderLabel_Click);
            // 
            // addDepartmentDropBox
            // 
            this.addDepartmentDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addDepartmentDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addDepartmentDropBox.FormattingEnabled = true;
            this.addDepartmentDropBox.Items.AddRange(new object[] {
            "Department Name here"});
            this.addDepartmentDropBox.Location = new System.Drawing.Point(406, 100);
            this.addDepartmentDropBox.Name = "addDepartmentDropBox";
            this.addDepartmentDropBox.Size = new System.Drawing.Size(320, 28);
            this.addDepartmentDropBox.TabIndex = 32;
            // 
            // addClassRoomEnter
            // 
            this.addClassRoomEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addClassRoomEnter.Location = new System.Drawing.Point(28, 100);
            this.addClassRoomEnter.Multiline = true;
            this.addClassRoomEnter.Name = "addClassRoomEnter";
            this.addClassRoomEnter.Size = new System.Drawing.Size(357, 30);
            this.addClassRoomEnter.TabIndex = 31;
            // 
            // addClassRoomNameLabel
            // 
            this.addClassRoomNameLabel.AutoSize = true;
            this.addClassRoomNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addClassRoomNameLabel.Location = new System.Drawing.Point(25, 77);
            this.addClassRoomNameLabel.Name = "addClassRoomNameLabel";
            this.addClassRoomNameLabel.Size = new System.Drawing.Size(58, 20);
            this.addClassRoomNameLabel.TabIndex = 30;
            this.addClassRoomNameLabel.Text = "Room:";
            // 
            // labelAddNewClass
            // 
            this.labelAddNewClass.AutoSize = true;
            this.labelAddNewClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddNewClass.Location = new System.Drawing.Point(282, 25);
            this.labelAddNewClass.Name = "labelAddNewClass";
            this.labelAddNewClass.Size = new System.Drawing.Size(210, 32);
            this.labelAddNewClass.TabIndex = 29;
            this.labelAddNewClass.Text = "Add new class";
            this.labelAddNewClass.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.exitButton.FlatAppearance.BorderSize = 0;
            this.exitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.ForeColor = System.Drawing.Color.White;
            this.exitButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.exitButton.IconColor = System.Drawing.Color.Black;
            this.exitButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.exitButton.Location = new System.Drawing.Point(470, 218);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(120, 40);
            this.exitButton.TabIndex = 28;
            this.exitButton.Text = "Cancel";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // createClass
            // 
            this.createClass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.createClass.FlatAppearance.BorderSize = 0;
            this.createClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createClass.ForeColor = System.Drawing.Color.White;
            this.createClass.IconChar = FontAwesome.Sharp.IconChar.None;
            this.createClass.IconColor = System.Drawing.Color.Black;
            this.createClass.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.createClass.Location = new System.Drawing.Point(606, 218);
            this.createClass.Name = "createClass";
            this.createClass.Size = new System.Drawing.Size(120, 40);
            this.createClass.TabIndex = 27;
            this.createClass.Text = "Create";
            this.createClass.UseVisualStyleBackColor = false;
            this.createClass.Click += new System.EventHandler(this.createTeacher_Click);
            // 
            // addGradeDropBox
            // 
            this.addGradeDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addGradeDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addGradeDropBox.FormattingEnabled = true;
            this.addGradeDropBox.Items.AddRange(new object[] {
            "Any grade"});
            this.addGradeDropBox.Location = new System.Drawing.Point(406, 160);
            this.addGradeDropBox.Name = "addGradeDropBox";
            this.addGradeDropBox.Size = new System.Drawing.Size(147, 28);
            this.addGradeDropBox.TabIndex = 49;
            // 
            // addSemesterDropBox
            // 
            this.addSemesterDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addSemesterDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSemesterDropBox.FormattingEnabled = true;
            this.addSemesterDropBox.Items.AddRange(new object[] {
            "Semester 1",
            "Semester 2"});
            this.addSemesterDropBox.Location = new System.Drawing.Point(576, 160);
            this.addSemesterDropBox.Name = "addSemesterDropBox";
            this.addSemesterDropBox.Size = new System.Drawing.Size(150, 28);
            this.addSemesterDropBox.TabIndex = 50;
            // 
            // dateTimePickerSchoolYears
            // 
            this.dateTimePickerSchoolYears.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerSchoolYears.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerSchoolYears.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dateTimePickerSchoolYears.Location = new System.Drawing.Point(28, 161);
            this.dateTimePickerSchoolYears.Name = "dateTimePickerSchoolYears";
            this.dateTimePickerSchoolYears.Size = new System.Drawing.Size(357, 27);
            this.dateTimePickerSchoolYears.TabIndex = 51;
            this.dateTimePickerSchoolYears.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // FormDialogAddClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 288);
            this.Controls.Add(this.dateTimePickerSchoolYears);
            this.Controls.Add(this.addSemesterDropBox);
            this.Controls.Add(this.addGradeDropBox);
            this.Controls.Add(this.addGradeLabel);
            this.Controls.Add(this.addSemesterLabel);
            this.Controls.Add(this.addSchoolyearLabel);
            this.Controls.Add(this.addClassDepartmentLabel);
            this.Controls.Add(this.addDepartmentDropBox);
            this.Controls.Add(this.addClassRoomEnter);
            this.Controls.Add(this.addClassRoomNameLabel);
            this.Controls.Add(this.labelAddNewClass);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.createClass);
            this.MaximumSize = new System.Drawing.Size(775, 335);
            this.MinimumSize = new System.Drawing.Size(775, 335);
            this.Name = "FormDialogAddClass";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Class";
            this.Load += new System.EventHandler(this.FormDialogAddClass_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label addGradeLabel;
        private System.Windows.Forms.Label addSemesterLabel;
        private System.Windows.Forms.Label addSchoolyearLabel;
        private System.Windows.Forms.Label addClassDepartmentLabel;
        private System.Windows.Forms.ComboBox addDepartmentDropBox;
        private System.Windows.Forms.TextBox addClassRoomEnter;
        private System.Windows.Forms.Label addClassRoomNameLabel;
        private System.Windows.Forms.Label labelAddNewClass;
        private FontAwesome.Sharp.IconButton exitButton;
        private FontAwesome.Sharp.IconButton createClass;
        private System.Windows.Forms.ComboBox addGradeDropBox;
        private System.Windows.Forms.ComboBox addSemesterDropBox;
        private System.Windows.Forms.DateTimePicker dateTimePickerSchoolYears;
    }
}