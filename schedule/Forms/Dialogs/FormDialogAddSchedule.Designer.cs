﻿
namespace schedule.Forms.Dialogs
{
    partial class FormDialogAddSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitButton = new FontAwesome.Sharp.IconButton();
            this.createSchedule = new FontAwesome.Sharp.IconButton();
            this.labelAddNewSchedule = new System.Windows.Forms.Label();
            this.selectRoomDropBox = new System.Windows.Forms.ComboBox();
            this.labelAddNewSubjectRoom = new System.Windows.Forms.Label();
            this.searchSubjectEnter = new System.Windows.Forms.TextBox();
            this.labelSearchSubject = new System.Windows.Forms.Label();
            this.subjectList = new System.Windows.Forms.ListBox();
            this.scheduleBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.exitButton.FlatAppearance.BorderSize = 0;
            this.exitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.ForeColor = System.Drawing.Color.White;
            this.exitButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.exitButton.IconColor = System.Drawing.Color.Black;
            this.exitButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.exitButton.Location = new System.Drawing.Point(1200, 25);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(120, 40);
            this.exitButton.TabIndex = 6;
            this.exitButton.Text = "Cancel";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // createSchedule
            // 
            this.createSchedule.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.createSchedule.FlatAppearance.BorderSize = 0;
            this.createSchedule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createSchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createSchedule.ForeColor = System.Drawing.Color.White;
            this.createSchedule.IconChar = FontAwesome.Sharp.IconChar.None;
            this.createSchedule.IconColor = System.Drawing.Color.Black;
            this.createSchedule.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.createSchedule.Location = new System.Drawing.Point(1336, 25);
            this.createSchedule.Name = "createSchedule";
            this.createSchedule.Size = new System.Drawing.Size(120, 40);
            this.createSchedule.TabIndex = 5;
            this.createSchedule.Text = "Create";
            this.createSchedule.UseVisualStyleBackColor = false;
            this.createSchedule.Click += new System.EventHandler(this.createSchedule_Click);
            // 
            // labelAddNewSchedule
            // 
            this.labelAddNewSchedule.AutoSize = true;
            this.labelAddNewSchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddNewSchedule.Location = new System.Drawing.Point(619, 25);
            this.labelAddNewSchedule.Name = "labelAddNewSchedule";
            this.labelAddNewSchedule.Size = new System.Drawing.Size(263, 32);
            this.labelAddNewSchedule.TabIndex = 53;
            this.labelAddNewSchedule.Text = "Add new schedule";
            this.labelAddNewSchedule.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // selectRoomDropBox
            // 
            this.selectRoomDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectRoomDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectRoomDropBox.FormattingEnabled = true;
            this.selectRoomDropBox.Items.AddRange(new object[] {
            "Room 1",
            "Room 2",
            "Room 3"});
            this.selectRoomDropBox.Location = new System.Drawing.Point(30, 95);
            this.selectRoomDropBox.Name = "selectRoomDropBox";
            this.selectRoomDropBox.Size = new System.Drawing.Size(220, 28);
            this.selectRoomDropBox.TabIndex = 54;
            // 
            // labelAddNewSubjectRoom
            // 
            this.labelAddNewSubjectRoom.AutoSize = true;
            this.labelAddNewSubjectRoom.Location = new System.Drawing.Point(29, 74);
            this.labelAddNewSubjectRoom.Name = "labelAddNewSubjectRoom";
            this.labelAddNewSubjectRoom.Size = new System.Drawing.Size(49, 17);
            this.labelAddNewSubjectRoom.TabIndex = 55;
            this.labelAddNewSubjectRoom.Text = "Room:";
            // 
            // searchSubjectEnter
            // 
            this.searchSubjectEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchSubjectEnter.Location = new System.Drawing.Point(30, 166);
            this.searchSubjectEnter.Name = "searchSubjectEnter";
            this.searchSubjectEnter.Size = new System.Drawing.Size(218, 27);
            this.searchSubjectEnter.TabIndex = 56;
            // 
            // labelSearchSubject
            // 
            this.labelSearchSubject.AutoSize = true;
            this.labelSearchSubject.Location = new System.Drawing.Point(27, 145);
            this.labelSearchSubject.Name = "labelSearchSubject";
            this.labelSearchSubject.Size = new System.Drawing.Size(108, 17);
            this.labelSearchSubject.TabIndex = 57;
            this.labelSearchSubject.Text = "Search Subject:";
            // 
            // subjectList
            // 
            this.subjectList.FormattingEnabled = true;
            this.subjectList.ItemHeight = 16;
            this.subjectList.Location = new System.Drawing.Point(30, 219);
            this.subjectList.Name = "subjectList";
            this.subjectList.Size = new System.Drawing.Size(218, 500);
            this.subjectList.TabIndex = 58;
            // 
            // scheduleBox
            // 
            this.scheduleBox.FormattingEnabled = true;
            this.scheduleBox.ItemHeight = 16;
            this.scheduleBox.Location = new System.Drawing.Point(271, 92);
            this.scheduleBox.Name = "scheduleBox";
            this.scheduleBox.Size = new System.Drawing.Size(1190, 628);
            this.scheduleBox.TabIndex = 59;
            // 
            // FormDialogAddSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1492, 753);
            this.Controls.Add(this.scheduleBox);
            this.Controls.Add(this.subjectList);
            this.Controls.Add(this.labelSearchSubject);
            this.Controls.Add(this.searchSubjectEnter);
            this.Controls.Add(this.labelAddNewSubjectRoom);
            this.Controls.Add(this.selectRoomDropBox);
            this.Controls.Add(this.labelAddNewSchedule);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.createSchedule);
            this.MaximumSize = new System.Drawing.Size(1510, 800);
            this.MinimumSize = new System.Drawing.Size(1510, 800);
            this.Name = "FormDialogAddSchedule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormDialogAddSchedule";
            this.Load += new System.EventHandler(this.FormDialogAddSchedule_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton exitButton;
        private FontAwesome.Sharp.IconButton createSchedule;
        private System.Windows.Forms.Label labelAddNewSchedule;
        private System.Windows.Forms.ComboBox selectRoomDropBox;
        private System.Windows.Forms.Label labelAddNewSubjectRoom;
        private System.Windows.Forms.TextBox searchSubjectEnter;
        private System.Windows.Forms.Label labelSearchSubject;
        private System.Windows.Forms.ListBox subjectList;
        private System.Windows.Forms.ListBox scheduleBox;
    }
}