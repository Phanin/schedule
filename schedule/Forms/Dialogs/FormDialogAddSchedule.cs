﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace schedule.Forms.Dialogs
{
    public partial class FormDialogAddSchedule : Form
    {
        public FormDialogAddSchedule()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
            FormSchedule.restrict = 0;
        }

        private void createSchedule_Click(object sender, EventArgs e)
        {
            this.Close();
            FormSchedule.restrict = 0;
        }

        private void FormDialogAddSchedule_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
        }
    }
}
