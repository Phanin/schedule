﻿
namespace schedule.Forms.Dialogs
{
    partial class FormDialogAddSubject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitButton = new FontAwesome.Sharp.IconButton();
            this.createSubject = new FontAwesome.Sharp.IconButton();
            this.addAvailableDayLabel = new System.Windows.Forms.Label();
            this.addSubjectTeacherLabel = new System.Windows.Forms.Label();
            this.addSubjectLabLabel = new System.Windows.Forms.Label();
            this.addSubjectLabDropBox = new System.Windows.Forms.ComboBox();
            this.addSubjectEnter = new System.Windows.Forms.TextBox();
            this.addSubjectNameLabel = new System.Windows.Forms.Label();
            this.labelAddNewSubject = new System.Windows.Forms.Label();
            this.addSubjectCreditDropBox = new System.Windows.Forms.ComboBox();
            this.labelSubjectCredit = new System.Windows.Forms.Label();
            this.AddTeacherDropBox = new System.Windows.Forms.ComboBox();
            this.labelAddSubjectAvailableTime = new System.Windows.Forms.Label();
            this.availableTimePicker = new System.Windows.Forms.DateTimePicker();
            this.addAvailableDayDropBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.exitButton.FlatAppearance.BorderSize = 0;
            this.exitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.ForeColor = System.Drawing.Color.White;
            this.exitButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.exitButton.IconColor = System.Drawing.Color.Black;
            this.exitButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.exitButton.Location = new System.Drawing.Point(467, 222);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(120, 40);
            this.exitButton.TabIndex = 8;
            this.exitButton.Text = "Cancel";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // createSubject
            // 
            this.createSubject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.createSubject.FlatAppearance.BorderSize = 0;
            this.createSubject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createSubject.ForeColor = System.Drawing.Color.White;
            this.createSubject.IconChar = FontAwesome.Sharp.IconChar.None;
            this.createSubject.IconColor = System.Drawing.Color.Black;
            this.createSubject.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.createSubject.Location = new System.Drawing.Point(603, 222);
            this.createSubject.Name = "createSubject";
            this.createSubject.Size = new System.Drawing.Size(120, 40);
            this.createSubject.TabIndex = 7;
            this.createSubject.Text = "Create";
            this.createSubject.UseVisualStyleBackColor = false;
            this.createSubject.Click += new System.EventHandler(this.createSubject_Click);
            // 
            // addAvailableDayLabel
            // 
            this.addAvailableDayLabel.AutoSize = true;
            this.addAvailableDayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addAvailableDayLabel.Location = new System.Drawing.Point(402, 141);
            this.addAvailableDayLabel.Name = "addAvailableDayLabel";
            this.addAvailableDayLabel.Size = new System.Drawing.Size(116, 20);
            this.addAvailableDayLabel.TabIndex = 59;
            this.addAvailableDayLabel.Text = "Available Day:";
            // 
            // addSubjectTeacherLabel
            // 
            this.addSubjectTeacherLabel.AutoSize = true;
            this.addSubjectTeacherLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSubjectTeacherLabel.Location = new System.Drawing.Point(25, 141);
            this.addSubjectTeacherLabel.Name = "addSubjectTeacherLabel";
            this.addSubjectTeacherLabel.Size = new System.Drawing.Size(75, 20);
            this.addSubjectTeacherLabel.TabIndex = 57;
            this.addSubjectTeacherLabel.Text = "Teacher:";
            // 
            // addSubjectLabLabel
            // 
            this.addSubjectLabLabel.AutoSize = true;
            this.addSubjectLabLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSubjectLabLabel.Location = new System.Drawing.Point(401, 81);
            this.addSubjectLabLabel.Name = "addSubjectLabLabel";
            this.addSubjectLabLabel.Size = new System.Drawing.Size(42, 20);
            this.addSubjectLabLabel.TabIndex = 56;
            this.addSubjectLabLabel.Text = "Lab:";
            // 
            // addSubjectLabDropBox
            // 
            this.addSubjectLabDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addSubjectLabDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSubjectLabDropBox.FormattingEnabled = true;
            this.addSubjectLabDropBox.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.addSubjectLabDropBox.Location = new System.Drawing.Point(405, 104);
            this.addSubjectLabDropBox.Name = "addSubjectLabDropBox";
            this.addSubjectLabDropBox.Size = new System.Drawing.Size(147, 28);
            this.addSubjectLabDropBox.TabIndex = 55;
            // 
            // addSubjectEnter
            // 
            this.addSubjectEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSubjectEnter.Location = new System.Drawing.Point(27, 104);
            this.addSubjectEnter.Multiline = true;
            this.addSubjectEnter.Name = "addSubjectEnter";
            this.addSubjectEnter.Size = new System.Drawing.Size(357, 30);
            this.addSubjectEnter.TabIndex = 54;
            // 
            // addSubjectNameLabel
            // 
            this.addSubjectNameLabel.AutoSize = true;
            this.addSubjectNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSubjectNameLabel.Location = new System.Drawing.Point(24, 81);
            this.addSubjectNameLabel.Name = "addSubjectNameLabel";
            this.addSubjectNameLabel.Size = new System.Drawing.Size(119, 20);
            this.addSubjectNameLabel.TabIndex = 53;
            this.addSubjectNameLabel.Text = "Subject Name:";
            // 
            // labelAddNewSubject
            // 
            this.labelAddNewSubject.AutoSize = true;
            this.labelAddNewSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddNewSubject.Location = new System.Drawing.Point(267, 25);
            this.labelAddNewSubject.Name = "labelAddNewSubject";
            this.labelAddNewSubject.Size = new System.Drawing.Size(238, 32);
            this.labelAddNewSubject.TabIndex = 52;
            this.labelAddNewSubject.Text = "Add new subject";
            this.labelAddNewSubject.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // addSubjectCreditDropBox
            // 
            this.addSubjectCreditDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addSubjectCreditDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSubjectCreditDropBox.FormattingEnabled = true;
            this.addSubjectCreditDropBox.Items.AddRange(new object[] {
            "1 (credit)",
            "2 (credits)",
            "3 (credits)",
            "4 (credits)"});
            this.addSubjectCreditDropBox.Location = new System.Drawing.Point(575, 104);
            this.addSubjectCreditDropBox.Name = "addSubjectCreditDropBox";
            this.addSubjectCreditDropBox.Size = new System.Drawing.Size(150, 28);
            this.addSubjectCreditDropBox.TabIndex = 64;
            // 
            // labelSubjectCredit
            // 
            this.labelSubjectCredit.AutoSize = true;
            this.labelSubjectCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubjectCredit.Location = new System.Drawing.Point(571, 81);
            this.labelSubjectCredit.Name = "labelSubjectCredit";
            this.labelSubjectCredit.Size = new System.Drawing.Size(68, 20);
            this.labelSubjectCredit.TabIndex = 63;
            this.labelSubjectCredit.Text = "Credits:";
            // 
            // AddTeacherDropBox
            // 
            this.AddTeacherDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AddTeacherDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddTeacherDropBox.FormattingEnabled = true;
            this.AddTeacherDropBox.Items.AddRange(new object[] {
            "First Teacher",
            "Second Teacher",
            "Third Teacher"});
            this.AddTeacherDropBox.Location = new System.Drawing.Point(29, 164);
            this.AddTeacherDropBox.Name = "AddTeacherDropBox";
            this.AddTeacherDropBox.Size = new System.Drawing.Size(165, 28);
            this.AddTeacherDropBox.TabIndex = 65;
            // 
            // labelAddSubjectAvailableTime
            // 
            this.labelAddSubjectAvailableTime.AutoSize = true;
            this.labelAddSubjectAvailableTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddSubjectAvailableTime.Location = new System.Drawing.Point(213, 141);
            this.labelAddSubjectAvailableTime.Name = "labelAddSubjectAvailableTime";
            this.labelAddSubjectAvailableTime.Size = new System.Drawing.Size(132, 20);
            this.labelAddSubjectAvailableTime.TabIndex = 67;
            this.labelAddSubjectAvailableTime.Text = "Available Times:";
            // 
            // availableTimePicker
            // 
            this.availableTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.availableTimePicker.Location = new System.Drawing.Point(217, 165);
            this.availableTimePicker.Name = "availableTimePicker";
            this.availableTimePicker.Size = new System.Drawing.Size(167, 27);
            this.availableTimePicker.TabIndex = 68;
            // 
            // addAvailableDayDropBox
            // 
            this.addAvailableDayDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addAvailableDayDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addAvailableDayDropBox.FormattingEnabled = true;
            this.addAvailableDayDropBox.Items.AddRange(new object[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"});
            this.addAvailableDayDropBox.Location = new System.Drawing.Point(406, 165);
            this.addAvailableDayDropBox.Name = "addAvailableDayDropBox";
            this.addAvailableDayDropBox.Size = new System.Drawing.Size(319, 28);
            this.addAvailableDayDropBox.TabIndex = 69;
            // 
            // FormDialogAddSubject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 288);
            this.Controls.Add(this.addAvailableDayDropBox);
            this.Controls.Add(this.availableTimePicker);
            this.Controls.Add(this.labelAddSubjectAvailableTime);
            this.Controls.Add(this.AddTeacherDropBox);
            this.Controls.Add(this.addSubjectCreditDropBox);
            this.Controls.Add(this.labelSubjectCredit);
            this.Controls.Add(this.addAvailableDayLabel);
            this.Controls.Add(this.addSubjectTeacherLabel);
            this.Controls.Add(this.addSubjectLabLabel);
            this.Controls.Add(this.addSubjectLabDropBox);
            this.Controls.Add(this.addSubjectEnter);
            this.Controls.Add(this.addSubjectNameLabel);
            this.Controls.Add(this.labelAddNewSubject);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.createSubject);
            this.Name = "FormDialogAddSubject";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Subject";
            this.Load += new System.EventHandler(this.FormDialogAddSubject_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton exitButton;
        private FontAwesome.Sharp.IconButton createSubject;
        private System.Windows.Forms.Label addAvailableDayLabel;
        private System.Windows.Forms.Label addSubjectTeacherLabel;
        private System.Windows.Forms.Label addSubjectLabLabel;
        private System.Windows.Forms.ComboBox addSubjectLabDropBox;
        private System.Windows.Forms.TextBox addSubjectEnter;
        private System.Windows.Forms.Label addSubjectNameLabel;
        private System.Windows.Forms.Label labelAddNewSubject;
        private System.Windows.Forms.ComboBox addSubjectCreditDropBox;
        private System.Windows.Forms.Label labelSubjectCredit;
        private System.Windows.Forms.ComboBox AddTeacherDropBox;
        private System.Windows.Forms.Label labelAddSubjectAvailableTime;
        private System.Windows.Forms.DateTimePicker availableTimePicker;
        private System.Windows.Forms.ComboBox addAvailableDayDropBox;
    }
}