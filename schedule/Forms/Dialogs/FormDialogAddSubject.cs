﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace schedule.Forms.Dialogs
{
    public partial class FormDialogAddSubject : Form
    {
        public FormDialogAddSubject()
        {
            InitializeComponent();
        }

        private void FormDialogAddSubject_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
            FormSubject.restrict = 0;
        }

        private void createSubject_Click(object sender, EventArgs e)
        {
            this.Close();
            FormSubject.restrict = 0;
        }

        private void addGradeDropBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
