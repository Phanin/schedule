﻿
namespace schedule.Forms.Dialogs
{
    partial class FormDialogAddTeacher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createTeacher = new FontAwesome.Sharp.IconButton();
            this.exitButton = new FontAwesome.Sharp.IconButton();
            this.labelAddNewTeacher = new System.Windows.Forms.Label();
            this.addNameTeacher = new System.Windows.Forms.TextBox();
            this.addTeacherNameLabel = new System.Windows.Forms.Label();
            this.addGenderDropBox = new System.Windows.Forms.ComboBox();
            this.addTeacherGenderLabel = new System.Windows.Forms.Label();
            this.addTeacherPositionLabel = new System.Windows.Forms.Label();
            this.addPositionDropBox = new System.Windows.Forms.ComboBox();
            this.addPhonenumberEnter = new System.Windows.Forms.TextBox();
            this.addTeacherPhoneLabel = new System.Windows.Forms.Label();
            this.addAddressEnter = new System.Windows.Forms.TextBox();
            this.addTeacherAddressLabel = new System.Windows.Forms.Label();
            this.addEmailEnter = new System.Windows.Forms.TextBox();
            this.addTeacherEmailLabel = new System.Windows.Forms.Label();
            this.addPasswordEnter = new System.Windows.Forms.TextBox();
            this.addTeachePasswordLabel = new System.Windows.Forms.Label();
            this.addDescEnter = new System.Windows.Forms.TextBox();
            this.addTeacherDescLabel = new System.Windows.Forms.Label();
            this.addTeacherStatusLabel = new System.Windows.Forms.Label();
            this.addTeacherStatusActive = new System.Windows.Forms.RadioButton();
            this.addTeacherStatusDisactive = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // createTeacher
            // 
            this.createTeacher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.createTeacher.FlatAppearance.BorderSize = 0;
            this.createTeacher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createTeacher.ForeColor = System.Drawing.Color.White;
            this.createTeacher.IconChar = FontAwesome.Sharp.IconChar.None;
            this.createTeacher.IconColor = System.Drawing.Color.Black;
            this.createTeacher.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.createTeacher.Location = new System.Drawing.Point(494, 461);
            this.createTeacher.Name = "createTeacher";
            this.createTeacher.Size = new System.Drawing.Size(120, 40);
            this.createTeacher.TabIndex = 3;
            this.createTeacher.Text = "Create";
            this.createTeacher.UseVisualStyleBackColor = false;
            this.createTeacher.Click += new System.EventHandler(this.buttonAddTeacher_Click);
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.exitButton.FlatAppearance.BorderSize = 0;
            this.exitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.ForeColor = System.Drawing.Color.White;
            this.exitButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.exitButton.IconColor = System.Drawing.Color.Black;
            this.exitButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.exitButton.Location = new System.Drawing.Point(358, 461);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(120, 40);
            this.exitButton.TabIndex = 4;
            this.exitButton.Text = "Cancel";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // labelAddNewTeacher
            // 
            this.labelAddNewTeacher.AutoSize = true;
            this.labelAddNewTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddNewTeacher.Location = new System.Drawing.Point(209, 25);
            this.labelAddNewTeacher.Name = "labelAddNewTeacher";
            this.labelAddNewTeacher.Size = new System.Drawing.Size(242, 32);
            this.labelAddNewTeacher.TabIndex = 5;
            this.labelAddNewTeacher.Text = "Add new teacher";
            this.labelAddNewTeacher.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // addNameTeacher
            // 
            this.addNameTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addNameTeacher.Location = new System.Drawing.Point(25, 100);
            this.addNameTeacher.Multiline = true;
            this.addNameTeacher.Name = "addNameTeacher";
            this.addNameTeacher.Size = new System.Drawing.Size(450, 30);
            this.addNameTeacher.TabIndex = 7;
            // 
            // addTeacherNameLabel
            // 
            this.addTeacherNameLabel.AutoSize = true;
            this.addTeacherNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherNameLabel.Location = new System.Drawing.Point(22, 77);
            this.addTeacherNameLabel.Name = "addTeacherNameLabel";
            this.addTeacherNameLabel.Size = new System.Drawing.Size(58, 20);
            this.addTeacherNameLabel.TabIndex = 6;
            this.addTeacherNameLabel.Text = "Name:";
            // 
            // addGenderDropBox
            // 
            this.addGenderDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addGenderDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addGenderDropBox.FormattingEnabled = true;
            this.addGenderDropBox.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.addGenderDropBox.Location = new System.Drawing.Point(492, 100);
            this.addGenderDropBox.Name = "addGenderDropBox";
            this.addGenderDropBox.Size = new System.Drawing.Size(121, 28);
            this.addGenderDropBox.TabIndex = 8;
            this.addGenderDropBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // addTeacherGenderLabel
            // 
            this.addTeacherGenderLabel.AutoSize = true;
            this.addTeacherGenderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherGenderLabel.Location = new System.Drawing.Point(488, 77);
            this.addTeacherGenderLabel.Name = "addTeacherGenderLabel";
            this.addTeacherGenderLabel.Size = new System.Drawing.Size(69, 20);
            this.addTeacherGenderLabel.TabIndex = 9;
            this.addTeacherGenderLabel.Text = "Gender:";
            // 
            // addTeacherPositionLabel
            // 
            this.addTeacherPositionLabel.AutoSize = true;
            this.addTeacherPositionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherPositionLabel.Location = new System.Drawing.Point(23, 137);
            this.addTeacherPositionLabel.Name = "addTeacherPositionLabel";
            this.addTeacherPositionLabel.Size = new System.Drawing.Size(83, 20);
            this.addTeacherPositionLabel.TabIndex = 10;
            this.addTeacherPositionLabel.Text = "Positions:";
            // 
            // addPositionDropBox
            // 
            this.addPositionDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addPositionDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addPositionDropBox.FormattingEnabled = true;
            this.addPositionDropBox.Items.AddRange(new object[] {
            "any position"});
            this.addPositionDropBox.Location = new System.Drawing.Point(27, 160);
            this.addPositionDropBox.Name = "addPositionDropBox";
            this.addPositionDropBox.Size = new System.Drawing.Size(270, 28);
            this.addPositionDropBox.TabIndex = 12;
            this.addPositionDropBox.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // addPhonenumberEnter
            // 
            this.addPhonenumberEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addPhonenumberEnter.Location = new System.Drawing.Point(316, 160);
            this.addPhonenumberEnter.Multiline = true;
            this.addPhonenumberEnter.Name = "addPhonenumberEnter";
            this.addPhonenumberEnter.Size = new System.Drawing.Size(297, 30);
            this.addPhonenumberEnter.TabIndex = 14;
            // 
            // addTeacherPhoneLabel
            // 
            this.addTeacherPhoneLabel.AutoSize = true;
            this.addTeacherPhoneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherPhoneLabel.Location = new System.Drawing.Point(312, 137);
            this.addTeacherPhoneLabel.Name = "addTeacherPhoneLabel";
            this.addTeacherPhoneLabel.Size = new System.Drawing.Size(95, 20);
            this.addTeacherPhoneLabel.TabIndex = 13;
            this.addTeacherPhoneLabel.Text = "Contact no:";
            // 
            // addAddressEnter
            // 
            this.addAddressEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addAddressEnter.Location = new System.Drawing.Point(27, 218);
            this.addAddressEnter.Multiline = true;
            this.addAddressEnter.Name = "addAddressEnter";
            this.addAddressEnter.Size = new System.Drawing.Size(586, 30);
            this.addAddressEnter.TabIndex = 16;
            // 
            // addTeacherAddressLabel
            // 
            this.addTeacherAddressLabel.AutoSize = true;
            this.addTeacherAddressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherAddressLabel.Location = new System.Drawing.Point(24, 195);
            this.addTeacherAddressLabel.Name = "addTeacherAddressLabel";
            this.addTeacherAddressLabel.Size = new System.Drawing.Size(76, 20);
            this.addTeacherAddressLabel.TabIndex = 15;
            this.addTeacherAddressLabel.Text = "Address:";
            // 
            // addEmailEnter
            // 
            this.addEmailEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addEmailEnter.Location = new System.Drawing.Point(28, 278);
            this.addEmailEnter.Multiline = true;
            this.addEmailEnter.Name = "addEmailEnter";
            this.addEmailEnter.Size = new System.Drawing.Size(269, 30);
            this.addEmailEnter.TabIndex = 18;
            // 
            // addTeacherEmailLabel
            // 
            this.addTeacherEmailLabel.AutoSize = true;
            this.addTeacherEmailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherEmailLabel.Location = new System.Drawing.Point(24, 255);
            this.addTeacherEmailLabel.Name = "addTeacherEmailLabel";
            this.addTeacherEmailLabel.Size = new System.Drawing.Size(56, 20);
            this.addTeacherEmailLabel.TabIndex = 17;
            this.addTeacherEmailLabel.Text = "Email:";
            // 
            // addPasswordEnter
            // 
            this.addPasswordEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addPasswordEnter.Location = new System.Drawing.Point(316, 278);
            this.addPasswordEnter.Multiline = true;
            this.addPasswordEnter.Name = "addPasswordEnter";
            this.addPasswordEnter.Size = new System.Drawing.Size(297, 30);
            this.addPasswordEnter.TabIndex = 20;
            // 
            // addTeachePasswordLabel
            // 
            this.addTeachePasswordLabel.AutoSize = true;
            this.addTeachePasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeachePasswordLabel.Location = new System.Drawing.Point(312, 255);
            this.addTeachePasswordLabel.Name = "addTeachePasswordLabel";
            this.addTeachePasswordLabel.Size = new System.Drawing.Size(88, 20);
            this.addTeachePasswordLabel.TabIndex = 19;
            this.addTeachePasswordLabel.Text = "Password:";
            // 
            // addDescEnter
            // 
            this.addDescEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addDescEnter.Location = new System.Drawing.Point(28, 336);
            this.addDescEnter.Multiline = true;
            this.addDescEnter.Name = "addDescEnter";
            this.addDescEnter.Size = new System.Drawing.Size(586, 100);
            this.addDescEnter.TabIndex = 22;
            // 
            // addTeacherDescLabel
            // 
            this.addTeacherDescLabel.AutoSize = true;
            this.addTeacherDescLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherDescLabel.Location = new System.Drawing.Point(25, 313);
            this.addTeacherDescLabel.Name = "addTeacherDescLabel";
            this.addTeacherDescLabel.Size = new System.Drawing.Size(76, 20);
            this.addTeacherDescLabel.TabIndex = 21;
            this.addTeacherDescLabel.Text = "Address:";
            // 
            // addTeacherStatusLabel
            // 
            this.addTeacherStatusLabel.AutoSize = true;
            this.addTeacherStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherStatusLabel.Location = new System.Drawing.Point(25, 442);
            this.addTeacherStatusLabel.Name = "addTeacherStatusLabel";
            this.addTeacherStatusLabel.Size = new System.Drawing.Size(62, 20);
            this.addTeacherStatusLabel.TabIndex = 23;
            this.addTeacherStatusLabel.Text = "Status:";
            // 
            // addTeacherStatusActive
            // 
            this.addTeacherStatusActive.AutoSize = true;
            this.addTeacherStatusActive.Checked = true;
            this.addTeacherStatusActive.Location = new System.Drawing.Point(29, 465);
            this.addTeacherStatusActive.Name = "addTeacherStatusActive";
            this.addTeacherStatusActive.Size = new System.Drawing.Size(67, 21);
            this.addTeacherStatusActive.TabIndex = 25;
            this.addTeacherStatusActive.TabStop = true;
            this.addTeacherStatusActive.Text = "Active";
            this.addTeacherStatusActive.UseVisualStyleBackColor = true;
            this.addTeacherStatusActive.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // addTeacherStatusDisactive
            // 
            this.addTeacherStatusDisactive.AutoSize = true;
            this.addTeacherStatusDisactive.Location = new System.Drawing.Point(128, 465);
            this.addTeacherStatusDisactive.Name = "addTeacherStatusDisactive";
            this.addTeacherStatusDisactive.Size = new System.Drawing.Size(86, 21);
            this.addTeacherStatusDisactive.TabIndex = 26;
            this.addTeacherStatusDisactive.Text = "Disactive";
            this.addTeacherStatusDisactive.UseVisualStyleBackColor = true;
            // 
            // FormDialogAddTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 527);
            this.Controls.Add(this.addTeacherStatusDisactive);
            this.Controls.Add(this.addTeacherStatusActive);
            this.Controls.Add(this.addTeacherStatusLabel);
            this.Controls.Add(this.addDescEnter);
            this.Controls.Add(this.addTeacherDescLabel);
            this.Controls.Add(this.addPasswordEnter);
            this.Controls.Add(this.addTeachePasswordLabel);
            this.Controls.Add(this.addEmailEnter);
            this.Controls.Add(this.addTeacherEmailLabel);
            this.Controls.Add(this.addAddressEnter);
            this.Controls.Add(this.addTeacherAddressLabel);
            this.Controls.Add(this.addPhonenumberEnter);
            this.Controls.Add(this.addTeacherPhoneLabel);
            this.Controls.Add(this.addPositionDropBox);
            this.Controls.Add(this.addTeacherPositionLabel);
            this.Controls.Add(this.addTeacherGenderLabel);
            this.Controls.Add(this.addGenderDropBox);
            this.Controls.Add(this.addNameTeacher);
            this.Controls.Add(this.addTeacherNameLabel);
            this.Controls.Add(this.labelAddNewTeacher);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.createTeacher);
            this.MaximumSize = new System.Drawing.Size(660, 574);
            this.MinimumSize = new System.Drawing.Size(660, 574);
            this.Name = "FormDialogAddTeacher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Teacher";
            this.Load += new System.EventHandler(this.FormDialogAddTeacher_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton createTeacher;
        private FontAwesome.Sharp.IconButton exitButton;
        private System.Windows.Forms.Label labelAddNewTeacher;
        private System.Windows.Forms.TextBox addNameTeacher;
        private System.Windows.Forms.Label addTeacherNameLabel;
        private System.Windows.Forms.ComboBox addGenderDropBox;
        private System.Windows.Forms.Label addTeacherGenderLabel;
        private System.Windows.Forms.Label addTeacherPositionLabel;
        private System.Windows.Forms.ComboBox addPositionDropBox;
        private System.Windows.Forms.TextBox addPhonenumberEnter;
        private System.Windows.Forms.Label addTeacherPhoneLabel;
        private System.Windows.Forms.TextBox addAddressEnter;
        private System.Windows.Forms.Label addTeacherAddressLabel;
        private System.Windows.Forms.TextBox addEmailEnter;
        private System.Windows.Forms.Label addTeacherEmailLabel;
        private System.Windows.Forms.TextBox addPasswordEnter;
        private System.Windows.Forms.Label addTeachePasswordLabel;
        private System.Windows.Forms.TextBox addDescEnter;
        private System.Windows.Forms.Label addTeacherDescLabel;
        private System.Windows.Forms.Label addTeacherStatusLabel;
        private System.Windows.Forms.RadioButton addTeacherStatusActive;
        private System.Windows.Forms.RadioButton addTeacherStatusDisactive;
    }
}