﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace schedule.Forms.Dialogs
{
    public partial class FormDialogAddTeacher : Form
    {
        public FormDialogAddTeacher()
        {
            InitializeComponent();
        }

        private void FormDialogAddTeacher_Load(object sender, EventArgs e)
        {
            ControlBox = false;
        }

        private void buttonAddTeacher_Click(object sender, EventArgs e)
        {
            this.Close();
            FormTeacher.restrict = 0;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
            FormTeacher.restrict = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
