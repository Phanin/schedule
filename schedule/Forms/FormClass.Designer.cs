﻿
namespace schedule.Forms
{
    partial class FormClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resetFieldClass = new FontAwesome.Sharp.IconButton();
            this.deleteClass = new FontAwesome.Sharp.IconButton();
            this.addDepartmentNameLabel = new System.Windows.Forms.Label();
            this.addDepartmentDropBox = new System.Windows.Forms.ComboBox();
            this.addRoomName = new System.Windows.Forms.TextBox();
            this.addRoomNameLabel = new System.Windows.Forms.Label();
            this.createClass = new FontAwesome.Sharp.IconButton();
            this.updateClass = new FontAwesome.Sharp.IconButton();
            this.teacherSummary = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherPhone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.positionTeacher = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBoxRoom = new System.Windows.Forms.GroupBox();
            this.teacherId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.addSchoolYearDropbox = new System.Windows.Forms.ComboBox();
            this.addSchoolYearLabel = new System.Windows.Forms.Label();
            this.addSemesterDropBox = new System.Windows.Forms.ComboBox();
            this.addGradeDropBox = new System.Windows.Forms.ComboBox();
            this.addGradeLabel = new System.Windows.Forms.Label();
            this.addSemesterLabel = new System.Windows.Forms.Label();
            this.classRoomList = new System.Windows.Forms.ListView();
            this.class_id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.class_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.department_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.class_semester = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.class_grade = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.class_school_year = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBoxRoom.SuspendLayout();
            this.SuspendLayout();
            // 
            // resetFieldClass
            // 
            this.resetFieldClass.BackColor = System.Drawing.Color.Green;
            this.resetFieldClass.FlatAppearance.BorderSize = 0;
            this.resetFieldClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetFieldClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetFieldClass.ForeColor = System.Drawing.Color.White;
            this.resetFieldClass.IconChar = FontAwesome.Sharp.IconChar.None;
            this.resetFieldClass.IconColor = System.Drawing.Color.Black;
            this.resetFieldClass.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.resetFieldClass.Location = new System.Drawing.Point(1039, 120);
            this.resetFieldClass.Name = "resetFieldClass";
            this.resetFieldClass.Size = new System.Drawing.Size(120, 40);
            this.resetFieldClass.TabIndex = 49;
            this.resetFieldClass.Text = "Reset";
            this.resetFieldClass.UseVisualStyleBackColor = false;
            this.resetFieldClass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.resetFieldClass_MouseClick);
            // 
            // deleteClass
            // 
            this.deleteClass.BackColor = System.Drawing.Color.Red;
            this.deleteClass.FlatAppearance.BorderSize = 0;
            this.deleteClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteClass.ForeColor = System.Drawing.Color.White;
            this.deleteClass.IconChar = FontAwesome.Sharp.IconChar.None;
            this.deleteClass.IconColor = System.Drawing.Color.Black;
            this.deleteClass.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.deleteClass.Location = new System.Drawing.Point(1448, 120);
            this.deleteClass.Name = "deleteClass";
            this.deleteClass.Size = new System.Drawing.Size(120, 40);
            this.deleteClass.TabIndex = 48;
            this.deleteClass.Text = "Delete";
            this.deleteClass.UseVisualStyleBackColor = false;
            this.deleteClass.Click += new System.EventHandler(this.deleteClass_Click);
            // 
            // addDepartmentNameLabel
            // 
            this.addDepartmentNameLabel.AutoSize = true;
            this.addDepartmentNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addDepartmentNameLabel.Location = new System.Drawing.Point(604, 40);
            this.addDepartmentNameLabel.Name = "addDepartmentNameLabel";
            this.addDepartmentNameLabel.Size = new System.Drawing.Size(148, 20);
            this.addDepartmentNameLabel.TabIndex = 32;
            this.addDepartmentNameLabel.Text = "Department name:";
            // 
            // addDepartmentDropBox
            // 
            this.addDepartmentDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addDepartmentDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addDepartmentDropBox.FormattingEnabled = true;
            this.addDepartmentDropBox.Items.AddRange(new object[] {
            "Information of Technology Engineering",
            "Telecommunication and Electronic Engineering",
            "Bioengineering"});
            this.addDepartmentDropBox.Location = new System.Drawing.Point(608, 65);
            this.addDepartmentDropBox.Name = "addDepartmentDropBox";
            this.addDepartmentDropBox.Size = new System.Drawing.Size(406, 28);
            this.addDepartmentDropBox.TabIndex = 31;
            // 
            // addRoomName
            // 
            this.addRoomName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addRoomName.Location = new System.Drawing.Point(35, 65);
            this.addRoomName.Multiline = true;
            this.addRoomName.Name = "addRoomName";
            this.addRoomName.Size = new System.Drawing.Size(550, 30);
            this.addRoomName.TabIndex = 30;
            // 
            // addRoomNameLabel
            // 
            this.addRoomNameLabel.AutoSize = true;
            this.addRoomNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addRoomNameLabel.Location = new System.Drawing.Point(32, 40);
            this.addRoomNameLabel.Name = "addRoomNameLabel";
            this.addRoomNameLabel.Size = new System.Drawing.Size(104, 20);
            this.addRoomNameLabel.TabIndex = 29;
            this.addRoomNameLabel.Text = "Room name:";
            // 
            // createClass
            // 
            this.createClass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.createClass.FlatAppearance.BorderSize = 0;
            this.createClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createClass.ForeColor = System.Drawing.Color.White;
            this.createClass.IconChar = FontAwesome.Sharp.IconChar.None;
            this.createClass.IconColor = System.Drawing.Color.Black;
            this.createClass.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.createClass.Location = new System.Drawing.Point(1177, 120);
            this.createClass.Name = "createClass";
            this.createClass.Size = new System.Drawing.Size(120, 40);
            this.createClass.TabIndex = 28;
            this.createClass.Text = "Create";
            this.createClass.UseVisualStyleBackColor = false;
            this.createClass.Click += new System.EventHandler(this.createClass_Click);
            // 
            // updateClass
            // 
            this.updateClass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.updateClass.FlatAppearance.BorderSize = 0;
            this.updateClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateClass.ForeColor = System.Drawing.Color.White;
            this.updateClass.IconChar = FontAwesome.Sharp.IconChar.None;
            this.updateClass.IconColor = System.Drawing.Color.Black;
            this.updateClass.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.updateClass.Location = new System.Drawing.Point(1313, 120);
            this.updateClass.Name = "updateClass";
            this.updateClass.Size = new System.Drawing.Size(120, 40);
            this.updateClass.TabIndex = 27;
            this.updateClass.Text = "Update";
            this.updateClass.UseVisualStyleBackColor = false;
            this.updateClass.Click += new System.EventHandler(this.updateClass_Click);
            // 
            // teacherSummary
            // 
            this.teacherSummary.Text = "Summary";
            this.teacherSummary.Width = 300;
            // 
            // teacherAddress
            // 
            this.teacherAddress.Text = "Address";
            this.teacherAddress.Width = 170;
            // 
            // teacherPhone
            // 
            this.teacherPhone.Text = "Phone";
            this.teacherPhone.Width = 180;
            // 
            // teacherEmail
            // 
            this.teacherEmail.Text = "Email";
            this.teacherEmail.Width = 190;
            // 
            // positionTeacher
            // 
            this.positionTeacher.Text = "Position";
            this.positionTeacher.Width = 90;
            // 
            // teacherName
            // 
            this.teacherName.Text = "Name";
            this.teacherName.Width = 170;
            // 
            // groupBoxRoom
            // 
            this.groupBoxRoom.Controls.Add(this.resetFieldClass);
            this.groupBoxRoom.Controls.Add(this.deleteClass);
            this.groupBoxRoom.Controls.Add(this.addDepartmentNameLabel);
            this.groupBoxRoom.Controls.Add(this.addDepartmentDropBox);
            this.groupBoxRoom.Controls.Add(this.addRoomName);
            this.groupBoxRoom.Controls.Add(this.addRoomNameLabel);
            this.groupBoxRoom.Controls.Add(this.createClass);
            this.groupBoxRoom.Controls.Add(this.updateClass);
            this.groupBoxRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxRoom.Location = new System.Drawing.Point(12, 12);
            this.groupBoxRoom.Name = "groupBoxRoom";
            this.groupBoxRoom.Size = new System.Drawing.Size(1620, 192);
            this.groupBoxRoom.TabIndex = 8;
            this.groupBoxRoom.TabStop = false;
            this.groupBoxRoom.Text = "Details";
            // 
            // teacherId
            // 
            this.teacherId.Text = "No";
            this.teacherId.Width = 40;
            // 
            // addSchoolYearDropbox
            // 
            this.addSchoolYearDropbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addSchoolYearDropbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSchoolYearDropbox.FormattingEnabled = true;
            this.addSchoolYearDropbox.Items.AddRange(new object[] {
            "1st Year",
            "2nd Year",
            "3rd Year",
            "4nd Year"});
            this.addSchoolYearDropbox.Location = new System.Drawing.Point(1051, 77);
            this.addSchoolYearDropbox.Name = "addSchoolYearDropbox";
            this.addSchoolYearDropbox.Size = new System.Drawing.Size(165, 28);
            this.addSchoolYearDropbox.TabIndex = 51;
            // 
            // addSchoolYearLabel
            // 
            this.addSchoolYearLabel.AutoSize = true;
            this.addSchoolYearLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSchoolYearLabel.Location = new System.Drawing.Point(1047, 52);
            this.addSchoolYearLabel.Name = "addSchoolYearLabel";
            this.addSchoolYearLabel.Size = new System.Drawing.Size(102, 20);
            this.addSchoolYearLabel.TabIndex = 50;
            this.addSchoolYearLabel.Text = "School year:";
            // 
            // addSemesterDropBox
            // 
            this.addSemesterDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addSemesterDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSemesterDropBox.FormattingEnabled = true;
            this.addSemesterDropBox.Items.AddRange(new object[] {
            "Semester 1",
            "Semester 2"});
            this.addSemesterDropBox.Location = new System.Drawing.Point(1430, 77);
            this.addSemesterDropBox.Name = "addSemesterDropBox";
            this.addSemesterDropBox.Size = new System.Drawing.Size(150, 28);
            this.addSemesterDropBox.TabIndex = 55;
            // 
            // addGradeDropBox
            // 
            this.addGradeDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addGradeDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addGradeDropBox.FormattingEnabled = true;
            this.addGradeDropBox.Items.AddRange(new object[] {
            "No grade"});
            this.addGradeDropBox.Location = new System.Drawing.Point(1246, 77);
            this.addGradeDropBox.Name = "addGradeDropBox";
            this.addGradeDropBox.Size = new System.Drawing.Size(160, 28);
            this.addGradeDropBox.TabIndex = 54;
            // 
            // addGradeLabel
            // 
            this.addGradeLabel.AutoSize = true;
            this.addGradeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addGradeLabel.Location = new System.Drawing.Point(1243, 54);
            this.addGradeLabel.Name = "addGradeLabel";
            this.addGradeLabel.Size = new System.Drawing.Size(60, 20);
            this.addGradeLabel.TabIndex = 53;
            this.addGradeLabel.Text = "Grade:";
            // 
            // addSemesterLabel
            // 
            this.addSemesterLabel.AutoSize = true;
            this.addSemesterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSemesterLabel.Location = new System.Drawing.Point(1426, 54);
            this.addSemesterLabel.Name = "addSemesterLabel";
            this.addSemesterLabel.Size = new System.Drawing.Size(86, 20);
            this.addSemesterLabel.TabIndex = 52;
            this.addSemesterLabel.Text = "Semester:";
            // 
            // classRoomList
            // 
            this.classRoomList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.class_id,
            this.class_name,
            this.department_name,
            this.class_semester,
            this.class_grade,
            this.class_school_year});
            this.classRoomList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classRoomList.FullRowSelect = true;
            this.classRoomList.HideSelection = false;
            this.classRoomList.Location = new System.Drawing.Point(12, 215);
            this.classRoomList.Name = "classRoomList";
            this.classRoomList.Size = new System.Drawing.Size(1620, 725);
            this.classRoomList.TabIndex = 56;
            this.classRoomList.UseCompatibleStateImageBehavior = false;
            this.classRoomList.View = System.Windows.Forms.View.Details;
            this.classRoomList.SelectedIndexChanged += new System.EventHandler(this.classRoomList_SelectedIndexChanged);
            // 
            // class_id
            // 
            this.class_id.Text = "Id";
            this.class_id.Width = 80;
            // 
            // class_name
            // 
            this.class_name.Text = "Class Name";
            this.class_name.Width = 300;
            // 
            // department_name
            // 
            this.department_name.Text = "Department Name";
            this.department_name.Width = 400;
            // 
            // class_semester
            // 
            this.class_semester.Text = "Semester";
            this.class_semester.Width = 150;
            // 
            // class_grade
            // 
            this.class_grade.Text = "Grade";
            this.class_grade.Width = 200;
            // 
            // class_school_year
            // 
            this.class_school_year.Text = "School Year";
            this.class_school_year.Width = 200;
            // 
            // FormClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1629, 906);
            this.Controls.Add(this.classRoomList);
            this.Controls.Add(this.addSemesterDropBox);
            this.Controls.Add(this.addGradeDropBox);
            this.Controls.Add(this.addGradeLabel);
            this.Controls.Add(this.addSemesterLabel);
            this.Controls.Add(this.addSchoolYearDropbox);
            this.Controls.Add(this.addSchoolYearLabel);
            this.Controls.Add(this.groupBoxRoom);
            this.Name = "FormClass";
            this.Text = "Class";
            this.Load += new System.EventHandler(this.FormClass_Load);
            this.groupBoxRoom.ResumeLayout(false);
            this.groupBoxRoom.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton resetFieldClass;
        private FontAwesome.Sharp.IconButton deleteClass;
        private System.Windows.Forms.Label addDepartmentNameLabel;
        private System.Windows.Forms.ComboBox addDepartmentDropBox;
        private System.Windows.Forms.TextBox addRoomName;
        private System.Windows.Forms.Label addRoomNameLabel;
        private FontAwesome.Sharp.IconButton createClass;
        private FontAwesome.Sharp.IconButton updateClass;
        private System.Windows.Forms.ColumnHeader teacherSummary;
        private System.Windows.Forms.ColumnHeader teacherAddress;
        private System.Windows.Forms.ColumnHeader teacherPhone;
        private System.Windows.Forms.ColumnHeader teacherEmail;
        private System.Windows.Forms.ColumnHeader positionTeacher;
        private System.Windows.Forms.ColumnHeader teacherName;
        private System.Windows.Forms.GroupBox groupBoxRoom;
        private System.Windows.Forms.ColumnHeader teacherId;
        private System.Windows.Forms.ComboBox addSchoolYearDropbox;
        private System.Windows.Forms.Label addSchoolYearLabel;
        private System.Windows.Forms.ComboBox addSemesterDropBox;
        private System.Windows.Forms.ComboBox addGradeDropBox;
        private System.Windows.Forms.Label addGradeLabel;
        private System.Windows.Forms.Label addSemesterLabel;
        private System.Windows.Forms.ListView classRoomList;
        private System.Windows.Forms.ColumnHeader class_id;
        private System.Windows.Forms.ColumnHeader class_name;
        private System.Windows.Forms.ColumnHeader department_name;
        private System.Windows.Forms.ColumnHeader class_semester;
        private System.Windows.Forms.ColumnHeader class_school_year;
        private System.Windows.Forms.ColumnHeader class_grade;
    }
}