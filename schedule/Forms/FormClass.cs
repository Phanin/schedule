﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using schedule.Class;

namespace schedule.Forms
{
    public partial class FormClass : Form
    {
        private ClassRoom currClassRoom;

        public FormClass()
        {
            InitializeComponent();
            ClassRoom.InitializeDbClassRoom();
        }

        private void LoadAll()
        {
            List<ClassRoom> classRooms = ClassRoom.GetClassRooms();

            classRoomList.Items.Clear();

            foreach (ClassRoom key in classRooms)
            {
                ListViewItem item = new ListViewItem(new String[] { key.Id.ToString(), key.className, key.departmentName, key.semester, key.grade, key.schoolYear });
                item.Tag = key;

                classRoomList.Items.Add(item);
            }
        }

        private void ResetAll()
        {
            addRoomName.Text = string.Empty;
            addDepartmentDropBox.SelectedIndex = -1;
            addGradeDropBox.SelectedIndex = -1;
            addSemesterDropBox.SelectedIndex = -1;
            addSchoolYearDropbox.SelectedIndex = -1;
        }

        private void FormClass_Load(object sender, EventArgs e)
        {
            LoadAll();
        }

        public static int restrict = 0;

        private void createClass_Click(object sender, EventArgs e)
        {
            String cname = addRoomName.Text; // cname = class name
            String dname = addDepartmentDropBox.Text; // dname = department name
            String gd = addGradeDropBox.Text;
            String smt = addSemesterDropBox.Text; // smt = semester
            String syear = addSchoolYearDropbox.Text;

            if (String.IsNullOrEmpty(cname) || String.IsNullOrEmpty(dname) || String.IsNullOrEmpty(smt) || String.IsNullOrEmpty(syear))
            {
                MessageBox.Show("Some of your field's empty!");
                return;
            }

            currClassRoom = ClassRoom.Insert(cname, dname, gd, smt, syear);
            LoadAll();
            ResetAll();
        }

        private void classRoomList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (classRoomList.SelectedItems.Count > 0)
            {
                ListViewItem item = classRoomList.SelectedItems[0];
                currClassRoom = (ClassRoom)item.Tag;

                int id = currClassRoom.Id;
                String cname = currClassRoom.className;
                String dname = currClassRoom.departmentName;
                String gd = currClassRoom.grade;
                String smt = currClassRoom.semester;
                String syear = currClassRoom.schoolYear;

                addRoomName.Text = cname;
                addDepartmentDropBox.Text = dname;
                addGradeDropBox.Text = gd;
                addSemesterDropBox.Text = smt;
                addSchoolYearDropbox.Text = syear;
            }
        }

        private void deleteClass_Click(object sender, EventArgs e)
        {
            if (currClassRoom == null)
            {
                MessageBox.Show("No class selected!");
                return;
            }

            currClassRoom.Delete();

            LoadAll();
            ResetAll();
        }

        private void updateClass_Click(object sender, EventArgs e)
        {
            String cname = addRoomName.Text;
            String dname = addDepartmentDropBox.Text;
            String gd = addGradeDropBox.Text;
            String smt = addSemesterDropBox.Text;
            String syear = addSchoolYearDropbox.Text;

            if (String.IsNullOrEmpty(cname) || String.IsNullOrEmpty(dname) || String.IsNullOrEmpty(smt) || String.IsNullOrEmpty(smt))
            {
                MessageBox.Show("No class selected!");
                return;
            }
            currClassRoom.Update(cname, dname, smt, syear);

            LoadAll();
            ResetAll();
        }

        private void resetFieldClass_MouseClick(object sender, MouseEventArgs e)
        {
            LoadAll();
            ResetAll();
        }
    }
}
