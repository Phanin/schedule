﻿
namespace schedule.Forms
{
    partial class FormPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPrintSchedule = new FontAwesome.Sharp.IconButton();
            this.labelPrintFilter = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // buttonPrintSchedule
            // 
            this.buttonPrintSchedule.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.buttonPrintSchedule.FlatAppearance.BorderSize = 0;
            this.buttonPrintSchedule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrintSchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrintSchedule.ForeColor = System.Drawing.Color.White;
            this.buttonPrintSchedule.IconChar = FontAwesome.Sharp.IconChar.None;
            this.buttonPrintSchedule.IconColor = System.Drawing.Color.Black;
            this.buttonPrintSchedule.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.buttonPrintSchedule.Location = new System.Drawing.Point(12, 12);
            this.buttonPrintSchedule.Name = "buttonPrintSchedule";
            this.buttonPrintSchedule.Size = new System.Drawing.Size(180, 40);
            this.buttonPrintSchedule.TabIndex = 7;
            this.buttonPrintSchedule.Text = "Print schedules";
            this.buttonPrintSchedule.UseVisualStyleBackColor = false;
            // 
            // labelPrintFilter
            // 
            this.labelPrintFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPrintFilter.AutoSize = true;
            this.labelPrintFilter.Location = new System.Drawing.Point(1171, 16);
            this.labelPrintFilter.Name = "labelPrintFilter";
            this.labelPrintFilter.Size = new System.Drawing.Size(62, 17);
            this.labelPrintFilter.TabIndex = 8;
            this.labelPrintFilter.Text = "Filter by:";
            this.labelPrintFilter.Click += new System.EventHandler(this.labelPrintFilter_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(1367, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(250, 28);
            this.comboBox1.TabIndex = 9;
            // 
            // comboBox2
            // 
            this.comboBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(1261, 12);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(100, 28);
            this.comboBox2.TabIndex = 10;
            // 
            // FormPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1629, 906);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelPrintFilter);
            this.Controls.Add(this.buttonPrintSchedule);
            this.Name = "FormPrint";
            this.Text = "Print";
            this.Load += new System.EventHandler(this.FormPrint_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton buttonPrintSchedule;
        private System.Windows.Forms.Label labelPrintFilter;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
    }
}