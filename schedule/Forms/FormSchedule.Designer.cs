﻿
namespace schedule.Forms
{
    partial class FormSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddSchedule = new FontAwesome.Sharp.IconButton();
            this.loadClassDropBox = new System.Windows.Forms.ComboBox();
            this.labelLoadClass = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonAddSchedule
            // 
            this.buttonAddSchedule.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.buttonAddSchedule.FlatAppearance.BorderSize = 0;
            this.buttonAddSchedule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddSchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddSchedule.ForeColor = System.Drawing.Color.White;
            this.buttonAddSchedule.IconChar = FontAwesome.Sharp.IconChar.None;
            this.buttonAddSchedule.IconColor = System.Drawing.Color.Black;
            this.buttonAddSchedule.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.buttonAddSchedule.Location = new System.Drawing.Point(12, 12);
            this.buttonAddSchedule.Name = "buttonAddSchedule";
            this.buttonAddSchedule.Size = new System.Drawing.Size(160, 40);
            this.buttonAddSchedule.TabIndex = 9;
            this.buttonAddSchedule.Text = "Add Schedule";
            this.buttonAddSchedule.UseVisualStyleBackColor = false;
            // 
            // loadClassDropBox
            // 
            this.loadClassDropBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.loadClassDropBox.DisplayMember = "xzx";
            this.loadClassDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.loadClassDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadClassDropBox.FormattingEnabled = true;
            this.loadClassDropBox.Items.AddRange(new object[] {
            "Class 1",
            "Class 2",
            "Class 3",
            "Class 4"});
            this.loadClassDropBox.Location = new System.Drawing.Point(1437, 16);
            this.loadClassDropBox.Name = "loadClassDropBox";
            this.loadClassDropBox.Size = new System.Drawing.Size(180, 28);
            this.loadClassDropBox.TabIndex = 13;
            this.loadClassDropBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // labelLoadClass
            // 
            this.labelLoadClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLoadClass.AutoSize = true;
            this.labelLoadClass.Location = new System.Drawing.Point(1323, 20);
            this.labelLoadClass.Name = "labelLoadClass";
            this.labelLoadClass.Size = new System.Drawing.Size(82, 17);
            this.labelLoadClass.TabIndex = 14;
            this.labelLoadClass.Text = "Load Class:";
            this.labelLoadClass.Click += new System.EventHandler(this.labelLoadClass_Click);
            // 
            // FormSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1629, 906);
            this.Controls.Add(this.labelLoadClass);
            this.Controls.Add(this.loadClassDropBox);
            this.Controls.Add(this.buttonAddSchedule);
            this.Name = "FormSchedule";
            this.Text = "Schedule";
            this.Load += new System.EventHandler(this.FormSchedule_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private FontAwesome.Sharp.IconButton buttonAddSchedule;
        private System.Windows.Forms.ComboBox loadClassDropBox;
        private System.Windows.Forms.Label labelLoadClass;
    }
}