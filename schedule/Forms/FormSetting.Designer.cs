﻿
namespace schedule.Forms
{
    partial class FormSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSettingUserName = new System.Windows.Forms.Label();
            this.settingUserName = new System.Windows.Forms.TextBox();
            this.labelSettingUserGender = new System.Windows.Forms.Label();
            this.userGenderDropBox = new System.Windows.Forms.ComboBox();
            this.labelSettingUserAddress = new System.Windows.Forms.Label();
            this.labelSettingUserEmail = new System.Windows.Forms.Label();
            this.labelSettingUserPassword = new System.Windows.Forms.Label();
            this.labelSettingUserStatus = new System.Windows.Forms.Label();
            this.settingUserAddress = new System.Windows.Forms.TextBox();
            this.settingUserEmail = new System.Windows.Forms.TextBox();
            this.settingUserPassword = new System.Windows.Forms.TextBox();
            this.settingUserContact = new System.Windows.Forms.TextBox();
            this.labelSettingUserContact = new System.Windows.Forms.Label();
            this.settingUserPasswordConfirmation = new System.Windows.Forms.TextBox();
            this.labelSettingUserPasswordConfirm = new System.Windows.Forms.Label();
            this.radioUserButtonActive = new System.Windows.Forms.RadioButton();
            this.radioUserButtonDisactive = new System.Windows.Forms.RadioButton();
            this.labelSettingUserDescription = new System.Windows.Forms.Label();
            this.settingUserTextBox = new System.Windows.Forms.TextBox();
            this.resetButton = new FontAwesome.Sharp.IconButton();
            this.editButton = new FontAwesome.Sharp.IconButton();
            this.userPositionDropBox = new System.Windows.Forms.ComboBox();
            this.userSettingPosition = new System.Windows.Forms.Label();
            this.groupBoxTeacher = new System.Windows.Forms.GroupBox();
            this.groupBoxTeacher.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelSettingUserName
            // 
            this.labelSettingUserName.AutoSize = true;
            this.labelSettingUserName.Location = new System.Drawing.Point(27, 41);
            this.labelSettingUserName.Name = "labelSettingUserName";
            this.labelSettingUserName.Size = new System.Drawing.Size(99, 20);
            this.labelSettingUserName.TabIndex = 0;
            this.labelSettingUserName.Text = "User Name:";
            // 
            // settingUserName
            // 
            this.settingUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingUserName.Location = new System.Drawing.Point(30, 64);
            this.settingUserName.Name = "settingUserName";
            this.settingUserName.Size = new System.Drawing.Size(289, 27);
            this.settingUserName.TabIndex = 1;
            this.settingUserName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.settingUserName_KeyPress);
            // 
            // labelSettingUserGender
            // 
            this.labelSettingUserGender.AutoSize = true;
            this.labelSettingUserGender.Location = new System.Drawing.Point(1314, 112);
            this.labelSettingUserGender.Name = "labelSettingUserGender";
            this.labelSettingUserGender.Size = new System.Drawing.Size(69, 20);
            this.labelSettingUserGender.TabIndex = 2;
            this.labelSettingUserGender.Text = "Gender:";
            // 
            // userGenderDropBox
            // 
            this.userGenderDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.userGenderDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userGenderDropBox.FormattingEnabled = true;
            this.userGenderDropBox.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.userGenderDropBox.Location = new System.Drawing.Point(1317, 137);
            this.userGenderDropBox.Name = "userGenderDropBox";
            this.userGenderDropBox.Size = new System.Drawing.Size(254, 28);
            this.userGenderDropBox.TabIndex = 3;
            // 
            // labelSettingUserAddress
            // 
            this.labelSettingUserAddress.AutoSize = true;
            this.labelSettingUserAddress.Location = new System.Drawing.Point(514, 114);
            this.labelSettingUserAddress.Name = "labelSettingUserAddress";
            this.labelSettingUserAddress.Size = new System.Drawing.Size(76, 20);
            this.labelSettingUserAddress.TabIndex = 4;
            this.labelSettingUserAddress.Text = "Address:";
            // 
            // labelSettingUserEmail
            // 
            this.labelSettingUserEmail.AutoSize = true;
            this.labelSettingUserEmail.Location = new System.Drawing.Point(348, 40);
            this.labelSettingUserEmail.Name = "labelSettingUserEmail";
            this.labelSettingUserEmail.Size = new System.Drawing.Size(56, 20);
            this.labelSettingUserEmail.TabIndex = 5;
            this.labelSettingUserEmail.Text = "Email:";
            // 
            // labelSettingUserPassword
            // 
            this.labelSettingUserPassword.AutoSize = true;
            this.labelSettingUserPassword.Location = new System.Drawing.Point(28, 114);
            this.labelSettingUserPassword.Name = "labelSettingUserPassword";
            this.labelSettingUserPassword.Size = new System.Drawing.Size(88, 20);
            this.labelSettingUserPassword.TabIndex = 6;
            this.labelSettingUserPassword.Text = "Password:";
            // 
            // labelSettingUserStatus
            // 
            this.labelSettingUserStatus.AutoSize = true;
            this.labelSettingUserStatus.Location = new System.Drawing.Point(27, 267);
            this.labelSettingUserStatus.Name = "labelSettingUserStatus";
            this.labelSettingUserStatus.Size = new System.Drawing.Size(62, 20);
            this.labelSettingUserStatus.TabIndex = 7;
            this.labelSettingUserStatus.Text = "Status:";
            // 
            // settingUserAddress
            // 
            this.settingUserAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingUserAddress.Location = new System.Drawing.Point(517, 138);
            this.settingUserAddress.Name = "settingUserAddress";
            this.settingUserAddress.Size = new System.Drawing.Size(764, 27);
            this.settingUserAddress.TabIndex = 8;
            this.settingUserAddress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.settingUserAddress_KeyPress);
            // 
            // settingUserEmail
            // 
            this.settingUserEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingUserEmail.Location = new System.Drawing.Point(351, 64);
            this.settingUserEmail.Name = "settingUserEmail";
            this.settingUserEmail.Size = new System.Drawing.Size(641, 27);
            this.settingUserEmail.TabIndex = 9;
            this.settingUserEmail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.settingUserEmail_KeyPress);
            // 
            // settingUserPassword
            // 
            this.settingUserPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingUserPassword.Location = new System.Drawing.Point(31, 138);
            this.settingUserPassword.Name = "settingUserPassword";
            this.settingUserPassword.Size = new System.Drawing.Size(444, 27);
            this.settingUserPassword.TabIndex = 10;
            this.settingUserPassword.Enter += new System.EventHandler(this.settingUserPassword_Enter);
            this.settingUserPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.settingUserPassword_KeyPress);
            this.settingUserPassword.Leave += new System.EventHandler(this.settingUserPassword_Leave);
            // 
            // settingUserContact
            // 
            this.settingUserContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingUserContact.Location = new System.Drawing.Point(1025, 64);
            this.settingUserContact.Name = "settingUserContact";
            this.settingUserContact.Size = new System.Drawing.Size(256, 27);
            this.settingUserContact.TabIndex = 12;
            this.settingUserContact.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.settingUserContact_KeyPress);
            // 
            // labelSettingUserContact
            // 
            this.labelSettingUserContact.AutoSize = true;
            this.labelSettingUserContact.Location = new System.Drawing.Point(1022, 39);
            this.labelSettingUserContact.Name = "labelSettingUserContact";
            this.labelSettingUserContact.Size = new System.Drawing.Size(95, 20);
            this.labelSettingUserContact.TabIndex = 11;
            this.labelSettingUserContact.Text = "Contact no:";
            // 
            // settingUserPasswordConfirmation
            // 
            this.settingUserPasswordConfirmation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingUserPasswordConfirmation.Location = new System.Drawing.Point(32, 213);
            this.settingUserPasswordConfirmation.Name = "settingUserPasswordConfirmation";
            this.settingUserPasswordConfirmation.Size = new System.Drawing.Size(443, 27);
            this.settingUserPasswordConfirmation.TabIndex = 14;
            this.settingUserPasswordConfirmation.Enter += new System.EventHandler(this.settingUserPasswordConfirmation_Enter);
            this.settingUserPasswordConfirmation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.settingUserPasswordConfirmation_KeyPress);
            this.settingUserPasswordConfirmation.Leave += new System.EventHandler(this.settingUserPasswordConfirmation_Leave);
            // 
            // labelSettingUserPasswordConfirm
            // 
            this.labelSettingUserPasswordConfirm.AutoSize = true;
            this.labelSettingUserPasswordConfirm.Location = new System.Drawing.Point(28, 188);
            this.labelSettingUserPasswordConfirm.Name = "labelSettingUserPasswordConfirm";
            this.labelSettingUserPasswordConfirm.Size = new System.Drawing.Size(185, 20);
            this.labelSettingUserPasswordConfirm.TabIndex = 13;
            this.labelSettingUserPasswordConfirm.Text = "Password confirmation:";
            // 
            // radioUserButtonActive
            // 
            this.radioUserButtonActive.AutoSize = true;
            this.radioUserButtonActive.Checked = true;
            this.radioUserButtonActive.Location = new System.Drawing.Point(132, 265);
            this.radioUserButtonActive.Name = "radioUserButtonActive";
            this.radioUserButtonActive.Size = new System.Drawing.Size(76, 24);
            this.radioUserButtonActive.TabIndex = 15;
            this.radioUserButtonActive.TabStop = true;
            this.radioUserButtonActive.Text = "Active";
            this.radioUserButtonActive.UseVisualStyleBackColor = true;
            // 
            // radioUserButtonDisactive
            // 
            this.radioUserButtonDisactive.AutoSize = true;
            this.radioUserButtonDisactive.Location = new System.Drawing.Point(253, 265);
            this.radioUserButtonDisactive.Name = "radioUserButtonDisactive";
            this.radioUserButtonDisactive.Size = new System.Drawing.Size(100, 24);
            this.radioUserButtonDisactive.TabIndex = 16;
            this.radioUserButtonDisactive.Text = "Disactive";
            this.radioUserButtonDisactive.UseVisualStyleBackColor = true;
            // 
            // labelSettingUserDescription
            // 
            this.labelSettingUserDescription.AutoSize = true;
            this.labelSettingUserDescription.Location = new System.Drawing.Point(515, 188);
            this.labelSettingUserDescription.Name = "labelSettingUserDescription";
            this.labelSettingUserDescription.Size = new System.Drawing.Size(100, 20);
            this.labelSettingUserDescription.TabIndex = 17;
            this.labelSettingUserDescription.Text = "Description:";
            // 
            // settingUserTextBox
            // 
            this.settingUserTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingUserTextBox.Location = new System.Drawing.Point(518, 213);
            this.settingUserTextBox.Multiline = true;
            this.settingUserTextBox.Name = "settingUserTextBox";
            this.settingUserTextBox.Size = new System.Drawing.Size(1053, 109);
            this.settingUserTextBox.TabIndex = 18;
            // 
            // resetButton
            // 
            this.resetButton.BackColor = System.Drawing.Color.Green;
            this.resetButton.FlatAppearance.BorderSize = 0;
            this.resetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetButton.ForeColor = System.Drawing.Color.White;
            this.resetButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.resetButton.IconColor = System.Drawing.Color.Black;
            this.resetButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.resetButton.Location = new System.Drawing.Point(1315, 352);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(120, 40);
            this.resetButton.TabIndex = 20;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = false;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // editButton
            // 
            this.editButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.editButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.editButton.FlatAppearance.BorderSize = 0;
            this.editButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editButton.ForeColor = System.Drawing.Color.White;
            this.editButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.editButton.IconColor = System.Drawing.Color.Black;
            this.editButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.editButton.Location = new System.Drawing.Point(1451, 352);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(120, 40);
            this.editButton.TabIndex = 19;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = false;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // userPositionDropBox
            // 
            this.userPositionDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.userPositionDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userPositionDropBox.FormattingEnabled = true;
            this.userPositionDropBox.Items.AddRange(new object[] {
            "Student",
            "Teacher"});
            this.userPositionDropBox.Location = new System.Drawing.Point(1316, 63);
            this.userPositionDropBox.Name = "userPositionDropBox";
            this.userPositionDropBox.Size = new System.Drawing.Size(255, 28);
            this.userPositionDropBox.TabIndex = 22;
            // 
            // userSettingPosition
            // 
            this.userSettingPosition.AutoSize = true;
            this.userSettingPosition.Location = new System.Drawing.Point(1313, 40);
            this.userSettingPosition.Name = "userSettingPosition";
            this.userSettingPosition.Size = new System.Drawing.Size(74, 20);
            this.userSettingPosition.TabIndex = 21;
            this.userSettingPosition.Text = "Position:";
            // 
            // groupBoxTeacher
            // 
            this.groupBoxTeacher.Controls.Add(this.resetButton);
            this.groupBoxTeacher.Controls.Add(this.userPositionDropBox);
            this.groupBoxTeacher.Controls.Add(this.editButton);
            this.groupBoxTeacher.Controls.Add(this.userSettingPosition);
            this.groupBoxTeacher.Controls.Add(this.radioUserButtonDisactive);
            this.groupBoxTeacher.Controls.Add(this.settingUserTextBox);
            this.groupBoxTeacher.Controls.Add(this.radioUserButtonActive);
            this.groupBoxTeacher.Controls.Add(this.labelSettingUserStatus);
            this.groupBoxTeacher.Controls.Add(this.labelSettingUserDescription);
            this.groupBoxTeacher.Controls.Add(this.settingUserAddress);
            this.groupBoxTeacher.Controls.Add(this.labelSettingUserGender);
            this.groupBoxTeacher.Controls.Add(this.userGenderDropBox);
            this.groupBoxTeacher.Controls.Add(this.labelSettingUserAddress);
            this.groupBoxTeacher.Controls.Add(this.settingUserPassword);
            this.groupBoxTeacher.Controls.Add(this.settingUserPasswordConfirmation);
            this.groupBoxTeacher.Controls.Add(this.labelSettingUserPassword);
            this.groupBoxTeacher.Controls.Add(this.labelSettingUserPasswordConfirm);
            this.groupBoxTeacher.Controls.Add(this.labelSettingUserEmail);
            this.groupBoxTeacher.Controls.Add(this.settingUserEmail);
            this.groupBoxTeacher.Controls.Add(this.labelSettingUserContact);
            this.groupBoxTeacher.Controls.Add(this.settingUserContact);
            this.groupBoxTeacher.Controls.Add(this.settingUserName);
            this.groupBoxTeacher.Controls.Add(this.labelSettingUserName);
            this.groupBoxTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxTeacher.Location = new System.Drawing.Point(12, 12);
            this.groupBoxTeacher.Name = "groupBoxTeacher";
            this.groupBoxTeacher.Size = new System.Drawing.Size(1605, 424);
            this.groupBoxTeacher.TabIndex = 23;
            this.groupBoxTeacher.TabStop = false;
            this.groupBoxTeacher.Text = "Details";
            // 
            // FormSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1629, 906);
            this.Controls.Add(this.groupBoxTeacher);
            this.Name = "FormSetting";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.FormSetting_Load);
            this.groupBoxTeacher.ResumeLayout(false);
            this.groupBoxTeacher.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelSettingUserName;
        public System.Windows.Forms.TextBox settingUserName;
        private System.Windows.Forms.Label labelSettingUserGender;
        private System.Windows.Forms.ComboBox userGenderDropBox;
        private System.Windows.Forms.Label labelSettingUserAddress;
        private System.Windows.Forms.Label labelSettingUserEmail;
        private System.Windows.Forms.Label labelSettingUserPassword;
        private System.Windows.Forms.Label labelSettingUserStatus;
        public System.Windows.Forms.TextBox settingUserAddress;
        public System.Windows.Forms.TextBox settingUserEmail;
        public System.Windows.Forms.TextBox settingUserPassword;
        public System.Windows.Forms.TextBox settingUserContact;
        private System.Windows.Forms.Label labelSettingUserContact;
        public System.Windows.Forms.TextBox settingUserPasswordConfirmation;
        private System.Windows.Forms.Label labelSettingUserPasswordConfirm;
        private System.Windows.Forms.RadioButton radioUserButtonActive;
        private System.Windows.Forms.RadioButton radioUserButtonDisactive;
        private System.Windows.Forms.Label labelSettingUserDescription;
        public System.Windows.Forms.TextBox settingUserTextBox;
        private FontAwesome.Sharp.IconButton resetButton;
        private FontAwesome.Sharp.IconButton editButton;
        private System.Windows.Forms.ComboBox userPositionDropBox;
        private System.Windows.Forms.Label userSettingPosition;
        private System.Windows.Forms.GroupBox groupBoxTeacher;
    }
}