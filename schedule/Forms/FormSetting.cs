﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using schedule.Class;

namespace schedule.Forms
{
    public partial class FormSetting : Form
    {
        public FormSetting()
        {
            InitializeComponent();
        }

        User user = new User();

        private void FormSetting_Load(object sender, EventArgs e)
        {
            LoadAll();
        }

        private void LoadAll()
        {
            DataTable table = user.getUserById(Globals.GlobalUserId);
            settingUserName.Text = table.Rows[0][2].ToString();
            settingUserEmail.Text = table.Rows[0][4].ToString();
            settingUserAddress.Text = table.Rows[0][13].ToString();
            //settingUserPassword.Text = table.Rows[0][6].ToString();
            //settingUserPasswordConfirmation.Text = table.Rows[0][6].ToString();
            settingUserContact.Text = table.Rows[0][11].ToString();
            settingUserTextBox.Text = table.Rows[0][14].ToString();
            string userStatus = table.Rows[0][7].ToString();
            string userGender = table.Rows[0][12].ToString();
            string userPosition = table.Rows[0][10].ToString();

            if (userPosition == "Teacher")
            {
                userPositionDropBox.SelectedIndex = 1;
            }
            else
            {
                userPositionDropBox.SelectedIndex = 0;
            }

            if (userStatus == "active")
            {
                radioUserButtonActive.Checked = true;
            }
            else
            {
                radioUserButtonDisactive.Checked = true;
            }

            if (userGender == "male")
            {
                userGenderDropBox.SelectedIndex = 0;
            }
            else
            {
                userGenderDropBox.SelectedIndex = 1;
            }
        }

        private void ResetAll()
        {
            settingUserName.Text = string.Empty;
            userGenderDropBox.SelectedIndex = -1;
            settingUserAddress.Text = string.Empty;
            settingUserEmail.Text = string.Empty;
            settingUserPassword.Text = string.Empty;
            settingUserPasswordConfirmation.Text = string.Empty;
            settingUserContact.Text = string.Empty;
            radioUserButtonActive.Checked = true;
            settingUserTextBox.Text = string.Empty;
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            ResetAll();
        }

        private void settingUserPassword_Enter(object sender, EventArgs e)
        {
            settingUserPassword.Text = "";
            settingUserPassword.ForeColor = Color.Black;
            settingUserPassword.UseSystemPasswordChar = true;
        }

        private void settingUserPassword_Leave(object sender, EventArgs e)
        {
            if (settingUserPassword.Text.Length == 0)
            {
                settingUserPassword.ForeColor = Color.Gray;
                settingUserPassword.Text = "Enter Password";
                settingUserPassword.UseSystemPasswordChar = false;
            }
        }

        private void settingUserPasswordConfirmation_Enter(object sender, EventArgs e)
        {
            settingUserPasswordConfirmation.Text = "";
            settingUserPasswordConfirmation.ForeColor = Color.Black;
            settingUserPasswordConfirmation.UseSystemPasswordChar = true;
        }

        private void settingUserPasswordConfirmation_Leave(object sender, EventArgs e)
        {
            if (settingUserPasswordConfirmation.Text.Length == 0)
            {
                settingUserPasswordConfirmation.ForeColor = Color.Gray;
                settingUserPasswordConfirmation.Text = "Enter confirmation password";
                settingUserPasswordConfirmation.UseSystemPasswordChar = false;
            }
        }

        private void settingUserPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true; // Mark the evenr as handled
            }
        }

        private void settingUserPasswordConfirmation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true; // Mark the evenr as handled
            }
        }

        private void settingUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true; // Mark the evenr as handled
            }
        }

        private void settingUserAddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true; // Mark the evenr as handled
            }
        }

        private void settingUserEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true; // Mark the evenr as handled
            }
        }

        private void settingUserContact_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true; // Mark the evenr as handled
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            int userid = Globals.GlobalUserId;
            string uname = settingUserName.Text;
            string email = settingUserEmail.Text;
            string pass = settingUserPassword.Text;
            string passConfirm = settingUserPasswordConfirmation.Text;
            string gender = userGenderDropBox.Text;
            string address = settingUserAddress.Text;
            string position = userPositionDropBox.Text;
            string pnumber = settingUserContact.Text;
            string smr = settingUserTextBox.Text;
            string status;

            if (radioUserButtonActive.Checked)
            {
                status = "active";
            }
            else
            {
                status = "disactive";
            }


            if (string.IsNullOrEmpty(uname) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(pass) || string.IsNullOrEmpty(gender))
            {
                MessageBox.Show("Required Fields: Username, Password, Email and Gender", "Edit Info", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (pass == passConfirm)
                {
                    if (user.updateUser(userid, uname, gender, address, email, pass, position, pnumber, status, smr))
                    {
                        MessageBox.Show("Your Information Has Been Updated", "Edit Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Error", "Edit Info", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Check your password again", "Edit Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
