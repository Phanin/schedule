﻿
namespace schedule.Forms
{
    partial class FormSubject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.is_lab = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.subject_credit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.subject_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.subject_id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.addIsLabDropbox = new System.Windows.Forms.ComboBox();
            this.addIsLabLabel = new System.Windows.Forms.Label();
            this.teacherId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBoxSubject = new System.Windows.Forms.GroupBox();
            this.labelTeacher = new System.Windows.Forms.Label();
            this.comboBoxTeachers = new System.Windows.Forms.ComboBox();
            this.resetFieldSubjects = new FontAwesome.Sharp.IconButton();
            this.deleteSubject = new FontAwesome.Sharp.IconButton();
            this.addCreditsLabel = new System.Windows.Forms.Label();
            this.addCreditsDropBox = new System.Windows.Forms.ComboBox();
            this.addSubjectName = new System.Windows.Forms.TextBox();
            this.addSubjectNameLabel = new System.Windows.Forms.Label();
            this.createSubject = new FontAwesome.Sharp.IconButton();
            this.updateSubject = new FontAwesome.Sharp.IconButton();
            this.teacherName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherPhone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherSummary = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.positionTeacher = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.subjectsList = new System.Windows.Forms.ListView();
            this.groupBoxSubject.SuspendLayout();
            this.SuspendLayout();
            // 
            // is_lab
            // 
            this.is_lab.Text = "Is Lab";
            this.is_lab.Width = 150;
            // 
            // subject_credit
            // 
            this.subject_credit.Text = "Subject Credits";
            this.subject_credit.Width = 150;
            // 
            // subject_name
            // 
            this.subject_name.Text = "Subject Name";
            this.subject_name.Width = 800;
            // 
            // subject_id
            // 
            this.subject_id.Text = "Id";
            this.subject_id.Width = 80;
            // 
            // addIsLabDropbox
            // 
            this.addIsLabDropbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addIsLabDropbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addIsLabDropbox.FormattingEnabled = true;
            this.addIsLabDropbox.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.addIsLabDropbox.Location = new System.Drawing.Point(920, 77);
            this.addIsLabDropbox.Name = "addIsLabDropbox";
            this.addIsLabDropbox.Size = new System.Drawing.Size(100, 28);
            this.addIsLabDropbox.TabIndex = 59;
            // 
            // addIsLabLabel
            // 
            this.addIsLabLabel.AutoSize = true;
            this.addIsLabLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addIsLabLabel.Location = new System.Drawing.Point(916, 52);
            this.addIsLabLabel.Name = "addIsLabLabel";
            this.addIsLabLabel.Size = new System.Drawing.Size(54, 20);
            this.addIsLabLabel.TabIndex = 58;
            this.addIsLabLabel.Text = "Is lab:";
            // 
            // teacherId
            // 
            this.teacherId.Text = "No";
            this.teacherId.Width = 40;
            // 
            // groupBoxSubject
            // 
            this.groupBoxSubject.Controls.Add(this.labelTeacher);
            this.groupBoxSubject.Controls.Add(this.comboBoxTeachers);
            this.groupBoxSubject.Controls.Add(this.resetFieldSubjects);
            this.groupBoxSubject.Controls.Add(this.deleteSubject);
            this.groupBoxSubject.Controls.Add(this.addCreditsLabel);
            this.groupBoxSubject.Controls.Add(this.addCreditsDropBox);
            this.groupBoxSubject.Controls.Add(this.addSubjectName);
            this.groupBoxSubject.Controls.Add(this.addSubjectNameLabel);
            this.groupBoxSubject.Controls.Add(this.createSubject);
            this.groupBoxSubject.Controls.Add(this.updateSubject);
            this.groupBoxSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxSubject.Location = new System.Drawing.Point(12, 12);
            this.groupBoxSubject.Name = "groupBoxSubject";
            this.groupBoxSubject.Size = new System.Drawing.Size(1620, 142);
            this.groupBoxSubject.TabIndex = 57;
            this.groupBoxSubject.TabStop = false;
            this.groupBoxSubject.Text = "Details";
            // 
            // labelTeacher
            // 
            this.labelTeacher.AutoSize = true;
            this.labelTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTeacher.Location = new System.Drawing.Point(466, 40);
            this.labelTeacher.Name = "labelTeacher";
            this.labelTeacher.Size = new System.Drawing.Size(75, 20);
            this.labelTeacher.TabIndex = 51;
            this.labelTeacher.Text = "Teacher:";
            // 
            // comboBoxTeachers
            // 
            this.comboBoxTeachers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTeachers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTeachers.FormattingEnabled = true;
            this.comboBoxTeachers.Items.AddRange(new object[] {
            "1.0",
            "1.5",
            "2.0",
            "2.5",
            "3.0"});
            this.comboBoxTeachers.Location = new System.Drawing.Point(470, 65);
            this.comboBoxTeachers.Name = "comboBoxTeachers";
            this.comboBoxTeachers.Size = new System.Drawing.Size(277, 28);
            this.comboBoxTeachers.TabIndex = 50;
            // 
            // resetFieldSubjects
            // 
            this.resetFieldSubjects.BackColor = System.Drawing.Color.Green;
            this.resetFieldSubjects.FlatAppearance.BorderSize = 0;
            this.resetFieldSubjects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetFieldSubjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetFieldSubjects.ForeColor = System.Drawing.Color.White;
            this.resetFieldSubjects.IconChar = FontAwesome.Sharp.IconChar.None;
            this.resetFieldSubjects.IconColor = System.Drawing.Color.Black;
            this.resetFieldSubjects.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.resetFieldSubjects.Location = new System.Drawing.Point(1039, 60);
            this.resetFieldSubjects.Name = "resetFieldSubjects";
            this.resetFieldSubjects.Size = new System.Drawing.Size(120, 40);
            this.resetFieldSubjects.TabIndex = 49;
            this.resetFieldSubjects.Text = "Reset";
            this.resetFieldSubjects.UseVisualStyleBackColor = false;
            this.resetFieldSubjects.Click += new System.EventHandler(this.resetFieldSubjects_Click);
            // 
            // deleteSubject
            // 
            this.deleteSubject.BackColor = System.Drawing.Color.Red;
            this.deleteSubject.FlatAppearance.BorderSize = 0;
            this.deleteSubject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSubject.ForeColor = System.Drawing.Color.White;
            this.deleteSubject.IconChar = FontAwesome.Sharp.IconChar.None;
            this.deleteSubject.IconColor = System.Drawing.Color.Black;
            this.deleteSubject.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.deleteSubject.Location = new System.Drawing.Point(1448, 60);
            this.deleteSubject.Name = "deleteSubject";
            this.deleteSubject.Size = new System.Drawing.Size(120, 40);
            this.deleteSubject.TabIndex = 48;
            this.deleteSubject.Text = "Delete";
            this.deleteSubject.UseVisualStyleBackColor = false;
            this.deleteSubject.Click += new System.EventHandler(this.deleteSubject_Click);
            // 
            // addCreditsLabel
            // 
            this.addCreditsLabel.AutoSize = true;
            this.addCreditsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addCreditsLabel.Location = new System.Drawing.Point(772, 40);
            this.addCreditsLabel.Name = "addCreditsLabel";
            this.addCreditsLabel.Size = new System.Drawing.Size(68, 20);
            this.addCreditsLabel.TabIndex = 32;
            this.addCreditsLabel.Text = "Credits:";
            // 
            // addCreditsDropBox
            // 
            this.addCreditsDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addCreditsDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addCreditsDropBox.FormattingEnabled = true;
            this.addCreditsDropBox.Items.AddRange(new object[] {
            "1.0",
            "1.5",
            "2.0",
            "2.5",
            "3.0"});
            this.addCreditsDropBox.Location = new System.Drawing.Point(776, 65);
            this.addCreditsDropBox.Name = "addCreditsDropBox";
            this.addCreditsDropBox.Size = new System.Drawing.Size(100, 28);
            this.addCreditsDropBox.TabIndex = 31;
            // 
            // addSubjectName
            // 
            this.addSubjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSubjectName.Location = new System.Drawing.Point(35, 65);
            this.addSubjectName.Multiline = true;
            this.addSubjectName.Name = "addSubjectName";
            this.addSubjectName.Size = new System.Drawing.Size(406, 30);
            this.addSubjectName.TabIndex = 30;
            // 
            // addSubjectNameLabel
            // 
            this.addSubjectNameLabel.AutoSize = true;
            this.addSubjectNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSubjectNameLabel.Location = new System.Drawing.Point(32, 40);
            this.addSubjectNameLabel.Name = "addSubjectNameLabel";
            this.addSubjectNameLabel.Size = new System.Drawing.Size(116, 20);
            this.addSubjectNameLabel.TabIndex = 29;
            this.addSubjectNameLabel.Text = "Subject name:";
            // 
            // createSubject
            // 
            this.createSubject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.createSubject.FlatAppearance.BorderSize = 0;
            this.createSubject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createSubject.ForeColor = System.Drawing.Color.White;
            this.createSubject.IconChar = FontAwesome.Sharp.IconChar.None;
            this.createSubject.IconColor = System.Drawing.Color.Black;
            this.createSubject.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.createSubject.Location = new System.Drawing.Point(1177, 60);
            this.createSubject.Name = "createSubject";
            this.createSubject.Size = new System.Drawing.Size(120, 40);
            this.createSubject.TabIndex = 28;
            this.createSubject.Text = "Create";
            this.createSubject.UseVisualStyleBackColor = false;
            this.createSubject.Click += new System.EventHandler(this.createSubject_Click);
            // 
            // updateSubject
            // 
            this.updateSubject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.updateSubject.FlatAppearance.BorderSize = 0;
            this.updateSubject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateSubject.ForeColor = System.Drawing.Color.White;
            this.updateSubject.IconChar = FontAwesome.Sharp.IconChar.None;
            this.updateSubject.IconColor = System.Drawing.Color.Black;
            this.updateSubject.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.updateSubject.Location = new System.Drawing.Point(1313, 60);
            this.updateSubject.Name = "updateSubject";
            this.updateSubject.Size = new System.Drawing.Size(120, 40);
            this.updateSubject.TabIndex = 27;
            this.updateSubject.Text = "Update";
            this.updateSubject.UseVisualStyleBackColor = false;
            this.updateSubject.Click += new System.EventHandler(this.updateSubject_Click);
            // 
            // teacherName
            // 
            this.teacherName.Text = "Name";
            this.teacherName.Width = 170;
            // 
            // teacherEmail
            // 
            this.teacherEmail.Text = "Email";
            this.teacherEmail.Width = 190;
            // 
            // teacherPhone
            // 
            this.teacherPhone.Text = "Phone";
            this.teacherPhone.Width = 180;
            // 
            // teacherAddress
            // 
            this.teacherAddress.Text = "Address";
            this.teacherAddress.Width = 170;
            // 
            // teacherSummary
            // 
            this.teacherSummary.Text = "Summary";
            this.teacherSummary.Width = 300;
            // 
            // positionTeacher
            // 
            this.positionTeacher.Text = "Position";
            this.positionTeacher.Width = 90;
            // 
            // subjectsList
            // 
            this.subjectsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.subject_id,
            this.subject_name,
            this.subject_credit,
            this.is_lab});
            this.subjectsList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectsList.FullRowSelect = true;
            this.subjectsList.HideSelection = false;
            this.subjectsList.Location = new System.Drawing.Point(12, 164);
            this.subjectsList.Name = "subjectsList";
            this.subjectsList.Size = new System.Drawing.Size(1620, 780);
            this.subjectsList.TabIndex = 64;
            this.subjectsList.UseCompatibleStateImageBehavior = false;
            this.subjectsList.View = System.Windows.Forms.View.Details;
            this.subjectsList.SelectedIndexChanged += new System.EventHandler(this.subjectsList_SelectedIndexChanged);
            // 
            // FormSubject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1629, 906);
            this.Controls.Add(this.addIsLabDropbox);
            this.Controls.Add(this.addIsLabLabel);
            this.Controls.Add(this.groupBoxSubject);
            this.Controls.Add(this.subjectsList);
            this.Name = "FormSubject";
            this.Text = "Subjects";
            this.Load += new System.EventHandler(this.FormSubject_Load);
            this.groupBoxSubject.ResumeLayout(false);
            this.groupBoxSubject.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ColumnHeader is_lab;
        private System.Windows.Forms.ColumnHeader subject_credit;
        private System.Windows.Forms.ColumnHeader subject_name;
        private System.Windows.Forms.ColumnHeader subject_id;
        private System.Windows.Forms.ComboBox addIsLabDropbox;
        private System.Windows.Forms.Label addIsLabLabel;
        private System.Windows.Forms.ColumnHeader teacherId;
        private System.Windows.Forms.GroupBox groupBoxSubject;
        private FontAwesome.Sharp.IconButton resetFieldSubjects;
        private FontAwesome.Sharp.IconButton deleteSubject;
        private System.Windows.Forms.Label addCreditsLabel;
        private System.Windows.Forms.ComboBox addCreditsDropBox;
        private System.Windows.Forms.TextBox addSubjectName;
        private System.Windows.Forms.Label addSubjectNameLabel;
        private FontAwesome.Sharp.IconButton createSubject;
        private FontAwesome.Sharp.IconButton updateSubject;
        private System.Windows.Forms.ColumnHeader teacherName;
        private System.Windows.Forms.ColumnHeader teacherEmail;
        private System.Windows.Forms.ColumnHeader teacherPhone;
        private System.Windows.Forms.ColumnHeader teacherAddress;
        private System.Windows.Forms.ColumnHeader teacherSummary;
        private System.Windows.Forms.ColumnHeader positionTeacher;
        private System.Windows.Forms.ListView subjectsList;
        private System.Windows.Forms.Label labelTeacher;
        private System.Windows.Forms.ComboBox comboBoxTeachers;
    }
}