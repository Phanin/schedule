﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using schedule.Class;
using System.Configuration;
using MySql.Data.MySqlClient;


namespace schedule.Forms
{
    public partial class FormSubject : Form
    {
        private Subject currSubject;

        public FormSubject()
        {
            InitializeComponent();
            Subject.InitializeDbSubject();
        }

        private void LoadAll()
        {
            List<Subject> subjects = Subject.GetSubjects();

            subjectsList.Items.Clear();

            foreach (Subject key in subjects)
            {
                ListViewItem item = new ListViewItem(new String[] { key.Id.ToString(), key.subjectName.ToString(), key.subjectCredits.ToString(), key.isLab.ToString() });
                item.Tag = key;

                subjectsList.Items.Add(item);
            }
        }

        private void ResetAll()
        {
            addSubjectName.Text = string.Empty;
            addCreditsDropBox.SelectedIndex = -1;
            addIsLabDropbox.SelectedIndex = -1;
        }

        private void FormSubject_Load(object sender, EventArgs e)
        {
            display_teacher();
            LoadAll();
        }

        private void createSubject_Click(object sender, EventArgs e)
        {
            MessageBox.Show(comboBoxTeachers.SelectedValue.ToString());
            String sjname = addSubjectName.Text;
            String sjcd = addCreditsDropBox.Text;
            String tid = comboBoxTeachers.SelectedValue.ToString(); // tid = teacher id
            String islab = "0";
            if (addIsLabDropbox.Text == "Yes")
                islab = "1";

            if (String.IsNullOrEmpty(sjname) || String.IsNullOrEmpty(sjcd.ToString()) || String.IsNullOrEmpty(islab))
            {
                MessageBox.Show("Some of your field's empty !");
                return;
            }

            currSubject = Subject.Insert(sjname, sjcd, islab, tid);
            
            LoadAll();
            ResetAll();
        }

        private void resetFieldSubjects_Click(object sender, EventArgs e)
        {
            LoadAll();
            ResetAll();
        }

        private void subjectsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (subjectsList.SelectedItems.Count > 0)
            {
                ListViewItem item = subjectsList.SelectedItems[0];
                currSubject = (Subject)item.Tag;

                int id = currSubject.Id;
                String sjname = currSubject.subjectName;
                String sjcd = currSubject.subjectCredits;
                String islab = currSubject.isLab;

                addSubjectName.Text = sjname;
                addCreditsDropBox.Text = sjcd.ToString();
                addIsLabDropbox.Text = islab;
            }
        }

        private void deleteSubject_Click(object sender, EventArgs e)
        {
            if (currSubject == null)
            {
                MessageBox.Show("No class selected!");
                return;
            }

            currSubject.Delete();

            LoadAll();
            ResetAll();
        }

        private void updateSubject_Click(object sender, EventArgs e)
        {
            String sjname = addSubjectName.Text;
            String sjcd = addCreditsDropBox.Text;
            String islab = "0";
            if (addIsLabDropbox.Text == "Yes")
                islab = "1";

            if (String.IsNullOrEmpty(sjname) || String.IsNullOrEmpty(sjcd) || String.IsNullOrEmpty(islab))
            {
                MessageBox.Show("No Subject selected!");
                return;
            }
            currSubject.Update(sjname, sjcd, islab);

            LoadAll();
            ResetAll();
        }
    
        private void display_teacher()
        {
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["scheduleConnection"].ConnectionString))
            {
                string query = "SELECT * from teachers, users WHERE teachers.user_id = users.id ORDER BY teachers.id";
                MySqlCommand command = new MySqlCommand(query, con);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                var dataTable = new DataTable();
                dataTable.Load(command.ExecuteReader());

                comboBoxTeachers.ValueMember = "id";
                comboBoxTeachers.DisplayMember = "name";
                comboBoxTeachers.DataSource = dataTable;
                comboBoxTeachers.SelectedIndex = -1;
            }
        }
    }
}
