﻿
namespace schedule.Forms
{
    partial class FormTeacher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teacherList = new System.Windows.Forms.ListView();
            this.teacherId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.positionTeacher = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherPhone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.teacherSummary = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBoxTeacher = new System.Windows.Forms.GroupBox();
            this.resetFieldTeacher = new FontAwesome.Sharp.IconButton();
            this.teacherDelete = new FontAwesome.Sharp.IconButton();
            this.addTeacherStatusDisactive = new System.Windows.Forms.RadioButton();
            this.addTeacherStatusActive = new System.Windows.Forms.RadioButton();
            this.addTeacherStatusLabel = new System.Windows.Forms.Label();
            this.addTeacherSummary = new System.Windows.Forms.TextBox();
            this.summaryLabel = new System.Windows.Forms.Label();
            this.addPasswordTeacher = new System.Windows.Forms.TextBox();
            this.addTeachePasswordLabel = new System.Windows.Forms.Label();
            this.addEmailTeacher = new System.Windows.Forms.TextBox();
            this.addTeacherEmailLabel = new System.Windows.Forms.Label();
            this.addAddressTeacher = new System.Windows.Forms.TextBox();
            this.addTeacherAddressLabel = new System.Windows.Forms.Label();
            this.addPhonenumberTeacher = new System.Windows.Forms.TextBox();
            this.addTeacherPhoneLabel = new System.Windows.Forms.Label();
            this.addPositionDropBox = new System.Windows.Forms.ComboBox();
            this.addTeacherPositionLabel = new System.Windows.Forms.Label();
            this.addTeacherGenderLabel = new System.Windows.Forms.Label();
            this.addGenderDropBox = new System.Windows.Forms.ComboBox();
            this.addNameTeacher = new System.Windows.Forms.TextBox();
            this.addTeacherNameLabel = new System.Windows.Forms.Label();
            this.teacherCreate = new FontAwesome.Sharp.IconButton();
            this.teacherUpdate = new FontAwesome.Sharp.IconButton();
            this.groupBoxTeacher.SuspendLayout();
            this.SuspendLayout();
            // 
            // teacherList
            // 
            this.teacherList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.teacherId,
            this.teacherName,
            this.positionTeacher,
            this.teacherEmail,
            this.teacherPhone,
            this.teacherAddress,
            this.teacherSummary});
            this.teacherList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teacherList.FullRowSelect = true;
            this.teacherList.HideSelection = false;
            this.teacherList.Location = new System.Drawing.Point(12, 365);
            this.teacherList.Name = "teacherList";
            this.teacherList.Size = new System.Drawing.Size(1622, 575);
            this.teacherList.TabIndex = 6;
            this.teacherList.UseCompatibleStateImageBehavior = false;
            this.teacherList.View = System.Windows.Forms.View.Details;
            this.teacherList.SelectedIndexChanged += new System.EventHandler(this.teacherList_SelectedIndexChanged);
            // 
            // teacherId
            // 
            this.teacherId.Text = "No";
            this.teacherId.Width = 40;
            // 
            // teacherName
            // 
            this.teacherName.Text = "Name";
            this.teacherName.Width = 170;
            // 
            // positionTeacher
            // 
            this.positionTeacher.Text = "Position";
            this.positionTeacher.Width = 90;
            // 
            // teacherEmail
            // 
            this.teacherEmail.Text = "Email";
            this.teacherEmail.Width = 190;
            // 
            // teacherPhone
            // 
            this.teacherPhone.Text = "Phone";
            this.teacherPhone.Width = 180;
            // 
            // teacherAddress
            // 
            this.teacherAddress.Text = "Address";
            this.teacherAddress.Width = 215;
            // 
            // teacherSummary
            // 
            this.teacherSummary.Text = "Summary";
            this.teacherSummary.Width = 330;
            // 
            // groupBoxTeacher
            // 
            this.groupBoxTeacher.Controls.Add(this.resetFieldTeacher);
            this.groupBoxTeacher.Controls.Add(this.teacherDelete);
            this.groupBoxTeacher.Controls.Add(this.addTeacherStatusDisactive);
            this.groupBoxTeacher.Controls.Add(this.addTeacherStatusActive);
            this.groupBoxTeacher.Controls.Add(this.addTeacherStatusLabel);
            this.groupBoxTeacher.Controls.Add(this.addTeacherSummary);
            this.groupBoxTeacher.Controls.Add(this.summaryLabel);
            this.groupBoxTeacher.Controls.Add(this.addPasswordTeacher);
            this.groupBoxTeacher.Controls.Add(this.addTeachePasswordLabel);
            this.groupBoxTeacher.Controls.Add(this.addEmailTeacher);
            this.groupBoxTeacher.Controls.Add(this.addTeacherEmailLabel);
            this.groupBoxTeacher.Controls.Add(this.addAddressTeacher);
            this.groupBoxTeacher.Controls.Add(this.addTeacherAddressLabel);
            this.groupBoxTeacher.Controls.Add(this.addPhonenumberTeacher);
            this.groupBoxTeacher.Controls.Add(this.addTeacherPhoneLabel);
            this.groupBoxTeacher.Controls.Add(this.addPositionDropBox);
            this.groupBoxTeacher.Controls.Add(this.addTeacherPositionLabel);
            this.groupBoxTeacher.Controls.Add(this.addTeacherGenderLabel);
            this.groupBoxTeacher.Controls.Add(this.addGenderDropBox);
            this.groupBoxTeacher.Controls.Add(this.addNameTeacher);
            this.groupBoxTeacher.Controls.Add(this.addTeacherNameLabel);
            this.groupBoxTeacher.Controls.Add(this.teacherCreate);
            this.groupBoxTeacher.Controls.Add(this.teacherUpdate);
            this.groupBoxTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxTeacher.Location = new System.Drawing.Point(12, 12);
            this.groupBoxTeacher.Name = "groupBoxTeacher";
            this.groupBoxTeacher.Size = new System.Drawing.Size(1622, 337);
            this.groupBoxTeacher.TabIndex = 7;
            this.groupBoxTeacher.TabStop = false;
            this.groupBoxTeacher.Text = "Details";
            // 
            // resetFieldTeacher
            // 
            this.resetFieldTeacher.BackColor = System.Drawing.Color.Green;
            this.resetFieldTeacher.FlatAppearance.BorderSize = 0;
            this.resetFieldTeacher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetFieldTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetFieldTeacher.ForeColor = System.Drawing.Color.White;
            this.resetFieldTeacher.IconChar = FontAwesome.Sharp.IconChar.None;
            this.resetFieldTeacher.IconColor = System.Drawing.Color.Black;
            this.resetFieldTeacher.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.resetFieldTeacher.Location = new System.Drawing.Point(1037, 269);
            this.resetFieldTeacher.Name = "resetFieldTeacher";
            this.resetFieldTeacher.Size = new System.Drawing.Size(120, 40);
            this.resetFieldTeacher.TabIndex = 49;
            this.resetFieldTeacher.Text = "Reset";
            this.resetFieldTeacher.UseVisualStyleBackColor = false;
            this.resetFieldTeacher.Click += new System.EventHandler(this.resetFieldTeacher_Click);
            // 
            // teacherDelete
            // 
            this.teacherDelete.BackColor = System.Drawing.Color.Red;
            this.teacherDelete.FlatAppearance.BorderSize = 0;
            this.teacherDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teacherDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teacherDelete.ForeColor = System.Drawing.Color.White;
            this.teacherDelete.IconChar = FontAwesome.Sharp.IconChar.None;
            this.teacherDelete.IconColor = System.Drawing.Color.Black;
            this.teacherDelete.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.teacherDelete.Location = new System.Drawing.Point(1446, 269);
            this.teacherDelete.Name = "teacherDelete";
            this.teacherDelete.Size = new System.Drawing.Size(120, 40);
            this.teacherDelete.TabIndex = 48;
            this.teacherDelete.Text = "Delete";
            this.teacherDelete.UseVisualStyleBackColor = false;
            this.teacherDelete.Click += new System.EventHandler(this.teacherDelete_Click);
            // 
            // addTeacherStatusDisactive
            // 
            this.addTeacherStatusDisactive.AutoSize = true;
            this.addTeacherStatusDisactive.Location = new System.Drawing.Point(224, 259);
            this.addTeacherStatusDisactive.Name = "addTeacherStatusDisactive";
            this.addTeacherStatusDisactive.Size = new System.Drawing.Size(100, 24);
            this.addTeacherStatusDisactive.TabIndex = 47;
            this.addTeacherStatusDisactive.Text = "Disactive";
            this.addTeacherStatusDisactive.UseVisualStyleBackColor = true;
            // 
            // addTeacherStatusActive
            // 
            this.addTeacherStatusActive.AutoSize = true;
            this.addTeacherStatusActive.Checked = true;
            this.addTeacherStatusActive.Location = new System.Drawing.Point(125, 259);
            this.addTeacherStatusActive.Name = "addTeacherStatusActive";
            this.addTeacherStatusActive.Size = new System.Drawing.Size(76, 24);
            this.addTeacherStatusActive.TabIndex = 46;
            this.addTeacherStatusActive.TabStop = true;
            this.addTeacherStatusActive.Text = "Active";
            this.addTeacherStatusActive.UseVisualStyleBackColor = true;
            // 
            // addTeacherStatusLabel
            // 
            this.addTeacherStatusLabel.AutoSize = true;
            this.addTeacherStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherStatusLabel.Location = new System.Drawing.Point(32, 261);
            this.addTeacherStatusLabel.Name = "addTeacherStatusLabel";
            this.addTeacherStatusLabel.Size = new System.Drawing.Size(62, 20);
            this.addTeacherStatusLabel.TabIndex = 45;
            this.addTeacherStatusLabel.Text = "Status:";
            // 
            // addTeacherSummary
            // 
            this.addTeacherSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherSummary.Location = new System.Drawing.Point(488, 139);
            this.addTeacherSummary.Multiline = true;
            this.addTeacherSummary.Name = "addTeacherSummary";
            this.addTeacherSummary.Size = new System.Drawing.Size(1077, 100);
            this.addTeacherSummary.TabIndex = 44;
            // 
            // summaryLabel
            // 
            this.summaryLabel.AutoSize = true;
            this.summaryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.summaryLabel.Location = new System.Drawing.Point(485, 114);
            this.summaryLabel.Name = "summaryLabel";
            this.summaryLabel.Size = new System.Drawing.Size(85, 20);
            this.summaryLabel.TabIndex = 43;
            this.summaryLabel.Text = "Summary:";
            // 
            // addPasswordTeacher
            // 
            this.addPasswordTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addPasswordTeacher.Location = new System.Drawing.Point(36, 139);
            this.addPasswordTeacher.Multiline = true;
            this.addPasswordTeacher.Name = "addPasswordTeacher";
            this.addPasswordTeacher.Size = new System.Drawing.Size(424, 30);
            this.addPasswordTeacher.TabIndex = 42;
            // 
            // addTeachePasswordLabel
            // 
            this.addTeachePasswordLabel.AutoSize = true;
            this.addTeachePasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeachePasswordLabel.Location = new System.Drawing.Point(32, 114);
            this.addTeachePasswordLabel.Name = "addTeachePasswordLabel";
            this.addTeachePasswordLabel.Size = new System.Drawing.Size(88, 20);
            this.addTeachePasswordLabel.TabIndex = 41;
            this.addTeachePasswordLabel.Text = "Password:";
            // 
            // addEmailTeacher
            // 
            this.addEmailTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addEmailTeacher.Location = new System.Drawing.Point(488, 65);
            this.addEmailTeacher.Multiline = true;
            this.addEmailTeacher.Name = "addEmailTeacher";
            this.addEmailTeacher.Size = new System.Drawing.Size(425, 30);
            this.addEmailTeacher.TabIndex = 40;
            // 
            // addTeacherEmailLabel
            // 
            this.addTeacherEmailLabel.AutoSize = true;
            this.addTeacherEmailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherEmailLabel.Location = new System.Drawing.Point(484, 40);
            this.addTeacherEmailLabel.Name = "addTeacherEmailLabel";
            this.addTeacherEmailLabel.Size = new System.Drawing.Size(56, 20);
            this.addTeacherEmailLabel.TabIndex = 39;
            this.addTeacherEmailLabel.Text = "Email:";
            // 
            // addAddressTeacher
            // 
            this.addAddressTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addAddressTeacher.Location = new System.Drawing.Point(35, 209);
            this.addAddressTeacher.Multiline = true;
            this.addAddressTeacher.Name = "addAddressTeacher";
            this.addAddressTeacher.Size = new System.Drawing.Size(423, 30);
            this.addAddressTeacher.TabIndex = 38;
            // 
            // addTeacherAddressLabel
            // 
            this.addTeacherAddressLabel.AutoSize = true;
            this.addTeacherAddressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherAddressLabel.Location = new System.Drawing.Point(32, 184);
            this.addTeacherAddressLabel.Name = "addTeacherAddressLabel";
            this.addTeacherAddressLabel.Size = new System.Drawing.Size(76, 20);
            this.addTeacherAddressLabel.TabIndex = 37;
            this.addTeacherAddressLabel.Text = "Address:";
            // 
            // addPhonenumberTeacher
            // 
            this.addPhonenumberTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addPhonenumberTeacher.Location = new System.Drawing.Point(939, 65);
            this.addPhonenumberTeacher.Multiline = true;
            this.addPhonenumberTeacher.Name = "addPhonenumberTeacher";
            this.addPhonenumberTeacher.Size = new System.Drawing.Size(250, 30);
            this.addPhonenumberTeacher.TabIndex = 36;
            // 
            // addTeacherPhoneLabel
            // 
            this.addTeacherPhoneLabel.AutoSize = true;
            this.addTeacherPhoneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherPhoneLabel.Location = new System.Drawing.Point(935, 40);
            this.addTeacherPhoneLabel.Name = "addTeacherPhoneLabel";
            this.addTeacherPhoneLabel.Size = new System.Drawing.Size(95, 20);
            this.addTeacherPhoneLabel.TabIndex = 35;
            this.addTeacherPhoneLabel.Text = "Contact no:";
            // 
            // addPositionDropBox
            // 
            this.addPositionDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addPositionDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addPositionDropBox.FormattingEnabled = true;
            this.addPositionDropBox.Items.AddRange(new object[] {
            "super admin",
            "admin",
            "teacher",
            "student"});
            this.addPositionDropBox.Location = new System.Drawing.Point(1216, 65);
            this.addPositionDropBox.Name = "addPositionDropBox";
            this.addPositionDropBox.Size = new System.Drawing.Size(200, 28);
            this.addPositionDropBox.TabIndex = 34;
            // 
            // addTeacherPositionLabel
            // 
            this.addTeacherPositionLabel.AutoSize = true;
            this.addTeacherPositionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherPositionLabel.Location = new System.Drawing.Point(1212, 40);
            this.addTeacherPositionLabel.Name = "addTeacherPositionLabel";
            this.addTeacherPositionLabel.Size = new System.Drawing.Size(83, 20);
            this.addTeacherPositionLabel.TabIndex = 33;
            this.addTeacherPositionLabel.Text = "Positions:";
            // 
            // addTeacherGenderLabel
            // 
            this.addTeacherGenderLabel.AutoSize = true;
            this.addTeacherGenderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherGenderLabel.Location = new System.Drawing.Point(1440, 40);
            this.addTeacherGenderLabel.Name = "addTeacherGenderLabel";
            this.addTeacherGenderLabel.Size = new System.Drawing.Size(69, 20);
            this.addTeacherGenderLabel.TabIndex = 32;
            this.addTeacherGenderLabel.Text = "Gender:";
            // 
            // addGenderDropBox
            // 
            this.addGenderDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addGenderDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addGenderDropBox.FormattingEnabled = true;
            this.addGenderDropBox.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.addGenderDropBox.Location = new System.Drawing.Point(1444, 65);
            this.addGenderDropBox.Name = "addGenderDropBox";
            this.addGenderDropBox.Size = new System.Drawing.Size(121, 28);
            this.addGenderDropBox.TabIndex = 31;
            // 
            // addNameTeacher
            // 
            this.addNameTeacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addNameTeacher.Location = new System.Drawing.Point(35, 65);
            this.addNameTeacher.Multiline = true;
            this.addNameTeacher.Name = "addNameTeacher";
            this.addNameTeacher.Size = new System.Drawing.Size(425, 30);
            this.addNameTeacher.TabIndex = 30;
            // 
            // addTeacherNameLabel
            // 
            this.addTeacherNameLabel.AutoSize = true;
            this.addTeacherNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addTeacherNameLabel.Location = new System.Drawing.Point(32, 40);
            this.addTeacherNameLabel.Name = "addTeacherNameLabel";
            this.addTeacherNameLabel.Size = new System.Drawing.Size(58, 20);
            this.addTeacherNameLabel.TabIndex = 29;
            this.addTeacherNameLabel.Text = "Name:";
            // 
            // teacherCreate
            // 
            this.teacherCreate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.teacherCreate.FlatAppearance.BorderSize = 0;
            this.teacherCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teacherCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teacherCreate.ForeColor = System.Drawing.Color.White;
            this.teacherCreate.IconChar = FontAwesome.Sharp.IconChar.None;
            this.teacherCreate.IconColor = System.Drawing.Color.Black;
            this.teacherCreate.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.teacherCreate.Location = new System.Drawing.Point(1175, 269);
            this.teacherCreate.Name = "teacherCreate";
            this.teacherCreate.Size = new System.Drawing.Size(120, 40);
            this.teacherCreate.TabIndex = 28;
            this.teacherCreate.Text = "Create";
            this.teacherCreate.UseVisualStyleBackColor = false;
            this.teacherCreate.Click += new System.EventHandler(this.createButton_Click);
            // 
            // teacherUpdate
            // 
            this.teacherUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.teacherUpdate.FlatAppearance.BorderSize = 0;
            this.teacherUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teacherUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teacherUpdate.ForeColor = System.Drawing.Color.White;
            this.teacherUpdate.IconChar = FontAwesome.Sharp.IconChar.None;
            this.teacherUpdate.IconColor = System.Drawing.Color.Black;
            this.teacherUpdate.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.teacherUpdate.Location = new System.Drawing.Point(1311, 269);
            this.teacherUpdate.Name = "teacherUpdate";
            this.teacherUpdate.Size = new System.Drawing.Size(120, 40);
            this.teacherUpdate.TabIndex = 27;
            this.teacherUpdate.Text = "Update";
            this.teacherUpdate.UseVisualStyleBackColor = false;
            this.teacherUpdate.Click += new System.EventHandler(this.updateTeacher_Click);
            // 
            // FormTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1629, 906);
            this.Controls.Add(this.groupBoxTeacher);
            this.Controls.Add(this.teacherList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormTeacher";
            this.Text = "Teachers";
            this.Load += new System.EventHandler(this.FormTeacher_Load);
            this.groupBoxTeacher.ResumeLayout(false);
            this.groupBoxTeacher.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListView teacherList;
        private System.Windows.Forms.ColumnHeader teacherId;
        private System.Windows.Forms.ColumnHeader teacherName;
        private System.Windows.Forms.ColumnHeader teacherEmail;
        private System.Windows.Forms.GroupBox groupBoxTeacher;
        private System.Windows.Forms.RadioButton addTeacherStatusDisactive;
        private System.Windows.Forms.RadioButton addTeacherStatusActive;
        private System.Windows.Forms.Label addTeacherStatusLabel;
        private System.Windows.Forms.TextBox addTeacherSummary;
        private System.Windows.Forms.Label summaryLabel;
        private System.Windows.Forms.TextBox addPasswordTeacher;
        private System.Windows.Forms.Label addTeachePasswordLabel;
        private System.Windows.Forms.TextBox addEmailTeacher;
        private System.Windows.Forms.Label addTeacherEmailLabel;
        private System.Windows.Forms.TextBox addAddressTeacher;
        private System.Windows.Forms.Label addTeacherAddressLabel;
        private System.Windows.Forms.TextBox addPhonenumberTeacher;
        private System.Windows.Forms.Label addTeacherPhoneLabel;
        private System.Windows.Forms.ComboBox addPositionDropBox;
        private System.Windows.Forms.Label addTeacherPositionLabel;
        private System.Windows.Forms.Label addTeacherGenderLabel;
        private System.Windows.Forms.ComboBox addGenderDropBox;
        private System.Windows.Forms.TextBox addNameTeacher;
        private System.Windows.Forms.Label addTeacherNameLabel;
        private FontAwesome.Sharp.IconButton teacherCreate;
        private FontAwesome.Sharp.IconButton teacherUpdate;
        private FontAwesome.Sharp.IconButton teacherDelete;
        private FontAwesome.Sharp.IconButton resetFieldTeacher;
        private System.Windows.Forms.ColumnHeader teacherPhone;
        private System.Windows.Forms.ColumnHeader teacherAddress;
        private System.Windows.Forms.ColumnHeader teacherSummary;
        private System.Windows.Forms.ColumnHeader positionTeacher;
    }
}