﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;
using schedule.Class;

namespace schedule.Forms
{
    public partial class FormTeacher : Form
    {
        private Teacher currTeacher;

        public FormTeacher()
        {
            InitializeComponent();
            Teacher.InitializeDbTeacher();
        }

        private void LoadAll()
        {
            List<Teacher> teachers = Teacher.GetTeachers();

            teacherList.Items.Clear();

            foreach (Teacher key in teachers)
            {
                ListViewItem item = new ListViewItem(new String[] { key.Id.ToString(), key.Username, key.Position, key.Email, key.Phone, key.Address, key.Summary });
                item.Tag = key;

                teacherList.Items.Add(item);
            }
        }

        private void ResetAll()
        {
            addNameTeacher.Text = string.Empty;
            addEmailTeacher.Text = string.Empty;
            addPasswordTeacher.Text = string.Empty;
            addPhonenumberTeacher.Text = string.Empty;
            addAddressTeacher.Text = string.Empty;
            addTeacherSummary.Text = string.Empty;
            addPositionDropBox.SelectedIndex = -1;
            addGenderDropBox.SelectedIndex = -1;
            addTeacherStatusActive.Checked = true;

        }

        private void FormTeacher_Load(object sender, EventArgs e)
        {
            LoadAll();
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            String usn = addNameTeacher.Text;
            String eml = addEmailTeacher.Text;
            String pwd = addPasswordTeacher.Text;
            String ph = addPhonenumberTeacher.Text;
            String ads = addAddressTeacher.Text;
            String smr = addTeacherSummary.Text;
            String utp = "2";
            String pst = addPositionDropBox.Text;
            String stt = "disactive";
            String sex = addGenderDropBox.Text;
            if (addTeacherStatusActive.Checked)
            {
                stt = "active";
            }

            if (String.IsNullOrEmpty(usn) || String.IsNullOrEmpty(eml) || String.IsNullOrEmpty(pwd) || String.IsNullOrEmpty(sex) || String.IsNullOrEmpty(pst))
            {
                MessageBox.Show("It's empty");
                return;
            }

            currTeacher = Teacher.Insert(usn, eml, pwd, pst, stt, utp, ph, sex, ads, smr);
            LoadAll();
            ResetAll();
        }

        private void updateTeacher_Click(object sender, EventArgs e)
        {
            String usn = addNameTeacher.Text;
            String eml = addEmailTeacher.Text;
            String pwd = addPasswordTeacher.Text;
            String ph = addPhonenumberTeacher.Text;
            String ads = addAddressTeacher.Text;
            String smr = addTeacherSummary.Text;
            String utp = "2";
            String pst = addPositionDropBox.Text;
            String stt = "disactive";
            String sex = addGenderDropBox.Text;
            if (addTeacherStatusActive.Checked)
            {
                stt = "active";
            }

            if (String.IsNullOrEmpty(usn) || String.IsNullOrEmpty(eml) || String.IsNullOrEmpty(ph) || String.IsNullOrEmpty(ads) || String.IsNullOrEmpty(smr))
            {
                MessageBox.Show("It's empty");
                return;
            }
            currTeacher.Update(usn, eml, pwd, pst, stt, utp, ph, sex, ads, smr);

            LoadAll();
            ResetAll();
        }

        private void teacherList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (teacherList.SelectedItems.Count > 0)
            {
                ListViewItem item = teacherList.SelectedItems[0];
                currTeacher = (Teacher)item.Tag;

                int id = currTeacher.Id;
                String usn = currTeacher.Username;
                String eml = currTeacher.Email;
                String ph = currTeacher.Phone;
                String ads = currTeacher.Address;
                String smr = currTeacher.Summary;
                String sex = currTeacher.Gender;
                String pst = currTeacher.Position;

                addNameTeacher.Text = usn;
                addEmailTeacher.Text = eml;
                addPhonenumberTeacher.Text = ph;
                addAddressTeacher.Text = ads;
                addTeacherSummary.Text = smr;
                addPositionDropBox.Text = pst;
                addGenderDropBox.Text = sex;
            }
        }

        private void teacherDelete_Click(object sender, EventArgs e)
        {
            if(currTeacher == null)
            {
                MessageBox.Show("No user selected!");
                return;
            }

            currTeacher.Delete();

            LoadAll();
            ResetAll();
        }

        private void resetFieldTeacher_Click(object sender, EventArgs e)
        {
            LoadAll();
            ResetAll();
        }
    }
}
