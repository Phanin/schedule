﻿
namespace schedule.Views
{
    partial class DashboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_menu = new System.Windows.Forms.Panel();
            this.settingButton = new FontAwesome.Sharp.IconButton();
            this.printButton = new FontAwesome.Sharp.IconButton();
            this.scheduleButton = new FontAwesome.Sharp.IconButton();
            this.subjectButton = new FontAwesome.Sharp.IconButton();
            this.classButton = new FontAwesome.Sharp.IconButton();
            this.teacherButton = new FontAwesome.Sharp.IconButton();
            this.sidebar_header = new System.Windows.Forms.Panel();
            this.sidebar_title = new System.Windows.Forms.Label();
            this.panel_title_bar = new System.Windows.Forms.Panel();
            this.icon_minimize = new FontAwesome.Sharp.IconPictureBox();
            this.icon_maximize = new FontAwesome.Sharp.IconPictureBox();
            this.icon_exit = new FontAwesome.Sharp.IconPictureBox();
            this.titleChildForm = new System.Windows.Forms.Label();
            this.iconCurrentChildForm = new FontAwesome.Sharp.IconPictureBox();
            this.panel_desktop = new System.Windows.Forms.Panel();
            this.panel_menu.SuspendLayout();
            this.sidebar_header.SuspendLayout();
            this.panel_title_bar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon_maximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconCurrentChildForm)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_menu
            // 
            this.panel_menu.BackColor = System.Drawing.Color.White;
            this.panel_menu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_menu.Controls.Add(this.settingButton);
            this.panel_menu.Controls.Add(this.printButton);
            this.panel_menu.Controls.Add(this.scheduleButton);
            this.panel_menu.Controls.Add(this.subjectButton);
            this.panel_menu.Controls.Add(this.classButton);
            this.panel_menu.Controls.Add(this.teacherButton);
            this.panel_menu.Controls.Add(this.sidebar_header);
            this.panel_menu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_menu.Location = new System.Drawing.Point(0, 0);
            this.panel_menu.Name = "panel_menu";
            this.panel_menu.Size = new System.Drawing.Size(255, 1033);
            this.panel_menu.TabIndex = 0;
            this.panel_menu.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_menu_Paint);
            // 
            // settingButton
            // 
            this.settingButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.settingButton.FlatAppearance.BorderSize = 0;
            this.settingButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settingButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.settingButton.IconChar = FontAwesome.Sharp.IconChar.Cogs;
            this.settingButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.settingButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.settingButton.IconSize = 32;
            this.settingButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.settingButton.Location = new System.Drawing.Point(0, 480);
            this.settingButton.Name = "settingButton";
            this.settingButton.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.settingButton.Size = new System.Drawing.Size(253, 80);
            this.settingButton.TabIndex = 6;
            this.settingButton.Text = "Settings";
            this.settingButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.settingButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.settingButton.UseVisualStyleBackColor = true;
            this.settingButton.Click += new System.EventHandler(this.iconButton6_Click);
            // 
            // printButton
            // 
            this.printButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.printButton.FlatAppearance.BorderSize = 0;
            this.printButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.printButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.printButton.IconChar = FontAwesome.Sharp.IconChar.Print;
            this.printButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.printButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.printButton.IconSize = 32;
            this.printButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.printButton.Location = new System.Drawing.Point(0, 400);
            this.printButton.Name = "printButton";
            this.printButton.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.printButton.Size = new System.Drawing.Size(253, 80);
            this.printButton.TabIndex = 5;
            this.printButton.Text = "Prints";
            this.printButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.printButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.iconButton5_Click);
            // 
            // scheduleButton
            // 
            this.scheduleButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.scheduleButton.FlatAppearance.BorderSize = 0;
            this.scheduleButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.scheduleButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.scheduleButton.IconChar = FontAwesome.Sharp.IconChar.Clock;
            this.scheduleButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.scheduleButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.scheduleButton.IconSize = 32;
            this.scheduleButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.scheduleButton.Location = new System.Drawing.Point(0, 320);
            this.scheduleButton.Name = "scheduleButton";
            this.scheduleButton.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.scheduleButton.Size = new System.Drawing.Size(253, 80);
            this.scheduleButton.TabIndex = 4;
            this.scheduleButton.Text = "Schedules";
            this.scheduleButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.scheduleButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.scheduleButton.UseVisualStyleBackColor = true;
            this.scheduleButton.Click += new System.EventHandler(this.iconButton4_Click);
            // 
            // subjectButton
            // 
            this.subjectButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.subjectButton.FlatAppearance.BorderSize = 0;
            this.subjectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subjectButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.subjectButton.IconChar = FontAwesome.Sharp.IconChar.Book;
            this.subjectButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.subjectButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.subjectButton.IconSize = 32;
            this.subjectButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subjectButton.Location = new System.Drawing.Point(0, 240);
            this.subjectButton.Name = "subjectButton";
            this.subjectButton.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.subjectButton.Size = new System.Drawing.Size(253, 80);
            this.subjectButton.TabIndex = 3;
            this.subjectButton.Text = "Subjects";
            this.subjectButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subjectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.subjectButton.UseVisualStyleBackColor = true;
            this.subjectButton.Click += new System.EventHandler(this.iconButton3_Click);
            // 
            // classButton
            // 
            this.classButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.classButton.FlatAppearance.BorderSize = 0;
            this.classButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.classButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.classButton.IconChar = FontAwesome.Sharp.IconChar.Building;
            this.classButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.classButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.classButton.IconSize = 32;
            this.classButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.classButton.Location = new System.Drawing.Point(0, 160);
            this.classButton.Name = "classButton";
            this.classButton.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.classButton.Size = new System.Drawing.Size(253, 80);
            this.classButton.TabIndex = 2;
            this.classButton.Text = "Class";
            this.classButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.classButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.classButton.UseVisualStyleBackColor = true;
            this.classButton.Click += new System.EventHandler(this.iconButton2_Click);
            // 
            // teacherButton
            // 
            this.teacherButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.teacherButton.FlatAppearance.BorderSize = 0;
            this.teacherButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teacherButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.teacherButton.IconChar = FontAwesome.Sharp.IconChar.ChalkboardTeacher;
            this.teacherButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.teacherButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.teacherButton.IconSize = 32;
            this.teacherButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.teacherButton.Location = new System.Drawing.Point(0, 80);
            this.teacherButton.Name = "teacherButton";
            this.teacherButton.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.teacherButton.Size = new System.Drawing.Size(253, 80);
            this.teacherButton.TabIndex = 1;
            this.teacherButton.Text = "Teachers";
            this.teacherButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.teacherButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.teacherButton.UseVisualStyleBackColor = true;
            this.teacherButton.Click += new System.EventHandler(this.iconButton1_Click);
            // 
            // sidebar_header
            // 
            this.sidebar_header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.sidebar_header.Controls.Add(this.sidebar_title);
            this.sidebar_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.sidebar_header.Location = new System.Drawing.Point(0, 0);
            this.sidebar_header.Name = "sidebar_header";
            this.sidebar_header.Size = new System.Drawing.Size(253, 80);
            this.sidebar_header.TabIndex = 0;
            this.sidebar_header.Paint += new System.Windows.Forms.PaintEventHandler(this.sidebar_header_Paint);
            // 
            // sidebar_title
            // 
            this.sidebar_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sidebar_title.ForeColor = System.Drawing.Color.White;
            this.sidebar_title.Location = new System.Drawing.Point(1, 0);
            this.sidebar_title.Name = "sidebar_title";
            this.sidebar_title.Size = new System.Drawing.Size(253, 80);
            this.sidebar_title.TabIndex = 0;
            this.sidebar_title.Text = "Class Schedule";
            this.sidebar_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sidebar_title.Click += new System.EventHandler(this.sidebar_title_Click);
            this.sidebar_title.MouseDown += new System.Windows.Forms.MouseEventHandler(this.sidebar_title_MouseDown);
            // 
            // panel_title_bar
            // 
            this.panel_title_bar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.panel_title_bar.Controls.Add(this.icon_minimize);
            this.panel_title_bar.Controls.Add(this.icon_maximize);
            this.panel_title_bar.Controls.Add(this.icon_exit);
            this.panel_title_bar.Controls.Add(this.titleChildForm);
            this.panel_title_bar.Controls.Add(this.iconCurrentChildForm);
            this.panel_title_bar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_title_bar.Location = new System.Drawing.Point(255, 0);
            this.panel_title_bar.Name = "panel_title_bar";
            this.panel_title_bar.Size = new System.Drawing.Size(1647, 80);
            this.panel_title_bar.TabIndex = 1;
            this.panel_title_bar.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_title_bar_Paint);
            this.panel_title_bar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_title_bar_MouseDown);
            // 
            // icon_minimize
            // 
            this.icon_minimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.icon_minimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.icon_minimize.IconChar = FontAwesome.Sharp.IconChar.WindowMinimize;
            this.icon_minimize.IconColor = System.Drawing.Color.White;
            this.icon_minimize.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.icon_minimize.Location = new System.Drawing.Point(1502, 8);
            this.icon_minimize.Name = "icon_minimize";
            this.icon_minimize.Size = new System.Drawing.Size(32, 32);
            this.icon_minimize.TabIndex = 4;
            this.icon_minimize.TabStop = false;
            this.icon_minimize.Click += new System.EventHandler(this.icon_minimize_Click);
            // 
            // icon_maximize
            // 
            this.icon_maximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.icon_maximize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.icon_maximize.IconChar = FontAwesome.Sharp.IconChar.WindowMaximize;
            this.icon_maximize.IconColor = System.Drawing.Color.White;
            this.icon_maximize.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.icon_maximize.Location = new System.Drawing.Point(1554, 8);
            this.icon_maximize.Name = "icon_maximize";
            this.icon_maximize.Size = new System.Drawing.Size(32, 32);
            this.icon_maximize.TabIndex = 3;
            this.icon_maximize.TabStop = false;
            this.icon_maximize.Click += new System.EventHandler(this.icon_maximize_Click);
            // 
            // icon_exit
            // 
            this.icon_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.icon_exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.icon_exit.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.icon_exit.IconColor = System.Drawing.Color.White;
            this.icon_exit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.icon_exit.Location = new System.Drawing.Point(1607, 8);
            this.icon_exit.Name = "icon_exit";
            this.icon_exit.Size = new System.Drawing.Size(32, 32);
            this.icon_exit.TabIndex = 2;
            this.icon_exit.TabStop = false;
            this.icon_exit.Click += new System.EventHandler(this.icon_exit_Click);
            // 
            // titleChildForm
            // 
            this.titleChildForm.AutoSize = true;
            this.titleChildForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleChildForm.ForeColor = System.Drawing.Color.White;
            this.titleChildForm.Location = new System.Drawing.Point(75, 31);
            this.titleChildForm.Name = "titleChildForm";
            this.titleChildForm.Size = new System.Drawing.Size(54, 20);
            this.titleChildForm.TabIndex = 1;
            this.titleChildForm.Text = "Home";
            this.titleChildForm.Click += new System.EventHandler(this.titleChildForm_Click);
            // 
            // iconCurrentChildForm
            // 
            this.iconCurrentChildForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.iconCurrentChildForm.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.iconCurrentChildForm.IconColor = System.Drawing.Color.White;
            this.iconCurrentChildForm.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconCurrentChildForm.IconSize = 50;
            this.iconCurrentChildForm.Location = new System.Drawing.Point(23, 22);
            this.iconCurrentChildForm.Name = "iconCurrentChildForm";
            this.iconCurrentChildForm.Size = new System.Drawing.Size(50, 50);
            this.iconCurrentChildForm.TabIndex = 0;
            this.iconCurrentChildForm.TabStop = false;
            // 
            // panel_desktop
            // 
            this.panel_desktop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_desktop.Location = new System.Drawing.Point(255, 80);
            this.panel_desktop.Name = "panel_desktop";
            this.panel_desktop.Size = new System.Drawing.Size(1647, 953);
            this.panel_desktop.TabIndex = 2;
            this.panel_desktop.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_desktop_Paint);
            // 
            // DashboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1902, 1033);
            this.Controls.Add(this.panel_desktop);
            this.Controls.Add(this.panel_title_bar);
            this.Controls.Add(this.panel_menu);
            this.Name = "DashboardForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DashboardForm";
            this.Load += new System.EventHandler(this.DashboardForm_Load);
            this.panel_menu.ResumeLayout(false);
            this.sidebar_header.ResumeLayout(false);
            this.panel_title_bar.ResumeLayout(false);
            this.panel_title_bar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon_maximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconCurrentChildForm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_menu;
        private System.Windows.Forms.Panel sidebar_header;
        private FontAwesome.Sharp.IconButton teacherButton;
        private FontAwesome.Sharp.IconButton settingButton;
        private FontAwesome.Sharp.IconButton printButton;
        private FontAwesome.Sharp.IconButton scheduleButton;
        private FontAwesome.Sharp.IconButton subjectButton;
        private FontAwesome.Sharp.IconButton classButton;
        private System.Windows.Forms.Label sidebar_title;
        private System.Windows.Forms.Panel panel_title_bar;
        private FontAwesome.Sharp.IconPictureBox iconCurrentChildForm;
        private System.Windows.Forms.Label titleChildForm;
        private System.Windows.Forms.Panel panel_desktop;
        private FontAwesome.Sharp.IconPictureBox icon_exit;
        private FontAwesome.Sharp.IconPictureBox icon_minimize;
        private FontAwesome.Sharp.IconPictureBox icon_maximize;
    }
}