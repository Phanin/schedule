﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;
using schedule.Class;
using schedule.Forms;

namespace schedule.Views
{
    public partial class DashboardForm : Form
    {
        // Fields
        private IconButton currentBtn;
        private Panel leftBorderBtn;
        private Form currentChildForm;

        // Constructor
        public DashboardForm()
        {
            InitializeComponent();
            leftBorderBtn = new Panel();
            leftBorderBtn.Size = new Size(7, 65);
            panel_menu.Controls.Add(leftBorderBtn);
            // Form
            this.Text = string.Empty;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }

        // Structs
        private struct lightBlueColor
        {
            public static Color color = Color.FromArgb(24, 144, 255);
        }

        // Method
        private void ActivateButton(object senderBtn, Color color)
        {
            if (senderBtn != null)
            {
                DisabledButton();
                // Button
                currentBtn = (IconButton)senderBtn;
                currentBtn.BackColor = Color.FromArgb(230, 247, 255);
                currentBtn.ForeColor = Color.FromArgb(24, 144, 255);
                currentBtn.TextAlign = ContentAlignment.MiddleCenter;
                currentBtn.IconColor = color;
                currentBtn.TextImageRelation = TextImageRelation.ImageBeforeText;
                currentBtn.ImageAlign = ContentAlignment.MiddleRight;
                // Left border button
                leftBorderBtn.BackColor = color;
                leftBorderBtn.Location = new Point(0, currentBtn.Location.Y);
                leftBorderBtn.Visible = true;
                leftBorderBtn.BringToFront();
                // Icon Current Child Form
                iconCurrentChildForm.IconChar = currentBtn.IconChar;
                iconCurrentChildForm.IconColor = Color.White;
            }
        }

        private void DisabledButton()
        {
            if (currentBtn != null)
            {
                currentBtn.BackColor = Color.White;
                currentBtn.ForeColor = Color.FromArgb(74, 85, 104);
                currentBtn.TextAlign = ContentAlignment.MiddleLeft;
                currentBtn.IconColor = Color.FromArgb(74, 85, 104);
                currentBtn.TextImageRelation = TextImageRelation.ImageBeforeText;
                currentBtn.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }

        public void OpenChildForm(Form childForm)
        {
            if(currentChildForm != null)
            {
                // Open only form
                currentChildForm.Close();
            }
            currentChildForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;

            panel_desktop.Controls.Add(childForm);
            panel_desktop.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            titleChildForm.Text = childForm.Text;
        }

        private void panel_menu_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void DashboardForm_Load(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.FormTeacher());
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, lightBlueColor.color);
            OpenChildForm(new Forms.FormTeacher());

        }

        private void sidebar_header_Paint(object sender, PaintEventArgs e)
        {

        }

        private void iconButton5_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, lightBlueColor.color);
            OpenChildForm(new Forms.FormPrint());
        }

        private void iconButton4_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, lightBlueColor.color);
            OpenChildForm(new Forms.FormSchedule());
        }

        private void iconButton3_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, lightBlueColor.color);
            OpenChildForm(new Forms.FormSubject());
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, lightBlueColor.color);
            OpenChildForm(new Forms.FormClass());
        }

        private void iconButton6_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, lightBlueColor.color);
            OpenChildForm(new Forms.FormSetting());
        }

        private void sidebar_title_Click(object sender, EventArgs e)
        {
            currentChildForm.Close();
            Reset();
        }

        private void Reset()
        {
            DisabledButton();
            leftBorderBtn.Visible = false;
            iconCurrentChildForm.IconChar = IconChar.Home;
            iconCurrentChildForm.IconColor = Color.White;
            titleChildForm.Text = "Home";
        }

        private void sidebar_title_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void panel_title_bar_Paint(object sender, PaintEventArgs e)
        {
            
        }

        // Drag Form
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panel_title_bar_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void titleChildForm_Click(object sender, EventArgs e)
        {
            
        }

        private void panel_desktop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void icon_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void icon_maximize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                WindowState = FormWindowState.Normal;
            }
        }

        private void icon_minimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
