﻿
namespace schedule.Views
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Panel login_background;
            this.label1 = new System.Windows.Forms.Label();
            this.login_submit = new System.Windows.Forms.Button();
            this.password_login = new System.Windows.Forms.TextBox();
            this.password_label = new System.Windows.Forms.Label();
            this.username_login = new System.Windows.Forms.TextBox();
            this.username_label = new System.Windows.Forms.Label();
            this.labelClose = new FontAwesome.Sharp.IconPictureBox();
            login_background = new System.Windows.Forms.Panel();
            login_background.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelClose)).BeginInit();
            this.SuspendLayout();
            // 
            // login_background
            // 
            login_background.BackColor = System.Drawing.Color.White;
            login_background.Controls.Add(this.label1);
            login_background.Controls.Add(this.login_submit);
            login_background.Controls.Add(this.password_login);
            login_background.Controls.Add(this.password_label);
            login_background.Controls.Add(this.username_login);
            login_background.Controls.Add(this.username_label);
            login_background.Location = new System.Drawing.Point(150, 138);
            login_background.Name = "login_background";
            login_background.Size = new System.Drawing.Size(508, 280);
            login_background.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(65, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(286, 32);
            this.label1.TabIndex = 4;
            this.label1.Text = "Login to your account";
            // 
            // login_submit
            // 
            this.login_submit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.login_submit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(144)))), ((int)(((byte)(220)))));
            this.login_submit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.login_submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login_submit.ForeColor = System.Drawing.Color.White;
            this.login_submit.Location = new System.Drawing.Point(321, 211);
            this.login_submit.Name = "login_submit";
            this.login_submit.Size = new System.Drawing.Size(155, 40);
            this.login_submit.TabIndex = 3;
            this.login_submit.Text = "login";
            this.login_submit.UseVisualStyleBackColor = false;
            this.login_submit.Click += new System.EventHandler(this.login_submit_Click);
            this.login_submit.MouseEnter += new System.EventHandler(this.login_submit_MouseEnter);
            // 
            // password_login
            // 
            this.password_login.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.password_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password_login.Location = new System.Drawing.Point(26, 163);
            this.password_login.Name = "password_login";
            this.password_login.Size = new System.Drawing.Size(450, 27);
            this.password_login.TabIndex = 2;
            this.password_login.UseSystemPasswordChar = true;
            this.password_login.WordWrap = false;
            this.password_login.Enter += new System.EventHandler(this.password_login_Enter);
            this.password_login.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.password_login_KeyPress);
            this.password_login.Leave += new System.EventHandler(this.password_login_Leave);
            // 
            // password_label
            // 
            this.password_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.password_label.AutoSize = true;
            this.password_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password_label.Location = new System.Drawing.Point(23, 140);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(88, 20);
            this.password_label.TabIndex = 3;
            this.password_label.Text = "Password:";
            // 
            // username_login
            // 
            this.username_login.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.username_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username_login.Location = new System.Drawing.Point(25, 99);
            this.username_login.Name = "username_login";
            this.username_login.Size = new System.Drawing.Size(450, 27);
            this.username_login.TabIndex = 1;
            this.username_login.WordWrap = false;
            this.username_login.TextChanged += new System.EventHandler(this.email_login_TextChanged);
            this.username_login.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.username_login_KeyPress);
            // 
            // username_label
            // 
            this.username_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.username_label.AutoSize = true;
            this.username_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username_label.Location = new System.Drawing.Point(21, 76);
            this.username_label.Name = "username_label";
            this.username_label.Size = new System.Drawing.Size(91, 20);
            this.username_label.TabIndex = 1;
            this.username_label.Text = "Username:";
            // 
            // labelClose
            // 
            this.labelClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.labelClose.ErrorImage = null;
            this.labelClose.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.labelClose.IconColor = System.Drawing.Color.White;
            this.labelClose.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.labelClose.Location = new System.Drawing.Point(738, 12);
            this.labelClose.Name = "labelClose";
            this.labelClose.Size = new System.Drawing.Size(32, 32);
            this.labelClose.TabIndex = 1;
            this.labelClose.TabStop = false;
            this.labelClose.Click += new System.EventHandler(this.iconPictureBox1_Click);
            this.labelClose.MouseEnter += new System.EventHandler(this.labelClose_MouseEnter);
            this.labelClose.MouseLeave += new System.EventHandler(this.labelClose_MouseLeave);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(85)))), ((int)(((byte)(104)))));
            this.ClientSize = new System.Drawing.Size(782, 553);
            this.Controls.Add(this.labelClose);
            this.Controls.Add(login_background);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoginForm";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            login_background.ResumeLayout(false);
            login_background.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label username_label;
        private System.Windows.Forms.TextBox username_login;
        private System.Windows.Forms.TextBox password_login;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.Button login_submit;
        private FontAwesome.Sharp.IconPictureBox labelClose;
        private System.Windows.Forms.Label label1;
    }
}