﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using schedule.Class;
using System.Configuration;

namespace schedule.Views
{
    public partial class LoginForm : Form
    {
        // Transfer to another form
        public static string T_username;
        public static string T_role;

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            username_login.Text = "";
            password_login.Text = "";
        }

        private void email_login_TextChanged(object sender, EventArgs e)
        {

        }

        private void login_submit_Click(object sender, EventArgs e)
        {
            bool verify = DataStatic.loginUser.User_verification(username_login.Text, password_login.Text);
            int userid = DataStatic.userId.Pass_userId(username_login.Text, password_login.Text);
            Globals.SetGlobalUserId(userid);
            if(verify)
            {
                MessageBox.Show("Successfully login!");
                DashboardForm dashboard = new DashboardForm();
                dashboard.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Username and password not correct!");
            }
        }

        private void iconPictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void labelClose_MouseEnter(object sender, EventArgs e)
        {
            labelClose.ForeColor = Color.Black;
            labelClose.Cursor = Cursors.Hand;
        }

        private void labelClose_MouseLeave(object sender, EventArgs e)
        {
            labelClose.ForeColor = Color.White;
        }

        private void login_submit_MouseEnter(object sender, EventArgs e)
        {
            login_submit.Cursor = Cursors.Hand;
        }

        private void password_login_Enter(object sender, EventArgs e)
        {
            password_login.Text = "";
            password_login.ForeColor = Color.Black;
            password_login.UseSystemPasswordChar = true;
        }

        private void password_login_Leave(object sender, EventArgs e)
        {
            if (password_login.Text.Length == 0)
            {
                password_login.ForeColor = Color.Gray;
                password_login.Text = "Enter password";
                password_login.UseSystemPasswordChar = false;
            }
        }

        private void password_login_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true; // Mark the event as handled
            }
        }

        private void username_login_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true; // Mark the event as handled
            }
        }
    }
}